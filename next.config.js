const withPWA = require("next-pwa");

module.exports = withPWA({
  future: {
    webpack5: true,
  },
  env: {
    ALGOLIA_APPLICATION_ID: "P10UCHC2PE",
    ALGOLIA_PUBLIC_KEY: "8abb59c9dc8df64b176a4d82a33f1a15",
  },
  pwa: {
    disable: process.env.NODE_ENV === "development",
    dest: "public",
  },
});
