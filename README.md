This is a starter template for [Learn Next.js](https://nextjs.org/learn).



## Faunda DB Cheatsheet
- Get item by index with term and sort ( data comes through as array)

`Map(
  Paginate(Match(Index("meals_search_by_groupRef_sort_by_name"), Ref(Collection("groups"), "290305145515803145"))),
  Lambda(["name", "ref" ], Get(Var("ref")))
)`


## Manifest generator
- https://github.com/onderceylan/pwa-asset-generator


    "generate-pwa": "pwa-asset-generator ./public/imgs/logo.svg ./public/imgs/manifest -p '15%' -b 'linear-gradient(180deg, #5B21B6 0%, #A78BFA 100%);' -m ./public/manifest.json --favicon"
// old logo with gradient => 