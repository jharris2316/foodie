import { LoggedInHome } from "components/home/LoggedInHome";
import { Card } from "components/shared/Card";
import { PageLayout } from "components/shared/PageLayout";
import { LoginForm } from "components/ui/Login";
import { useIsLoggedIn } from "lib/hooks/user";
import Head from "next/head";

export default function Home() {
  const isLoggedIn = useIsLoggedIn();
  return (
    <>
      <Head>
        <title>Feed.Me.</title>
      </Head>

      <PageLayout>
        {isLoggedIn ? (
          <LoggedInHome />
        ) : (
          <div className="h-screen flex flex-col justify-center -mt-16">
            <Card>
              <Card.Body>
                <LoginForm />
              </Card.Body>
            </Card>
          </div>
        )}
      </PageLayout>
    </>
  );
}
