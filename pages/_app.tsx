import { Header } from "components/shared/Header";
import { useRouter } from "next/router";
import { SWRConfig } from "swr";
import { jsonFetcher } from "utils/http";
import "../styles/global.scss";
import { RecoilRoot } from "recoil";
import { Boot } from "components/shared/Boot";
import { Routes } from "lib/router";
import Head from "next/head";
import { Toast } from "components/shared/Toast/Toast";
import { AnimatePresence } from "framer-motion";

export default function App({ Component, pageProps }) {
  const router = useRouter();

  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
        />
      </Head>
      <RecoilRoot>
        <SWRConfig
          value={{
            shouldRetryOnError: false,
            fetcher: jsonFetcher,
            onError: (err, key, config) => {
              switch (err.status) {
                case 401: {
                  router.push(Routes.Login);
                  return;
                }
                default:
                  break;
              }
            },
          }}>
          <Boot>
            <Wrap>
              <Header />
              <AnimatePresence>
                <Component {...pageProps} />
              </AnimatePresence>
              <Toast />
            </Wrap>
          </Boot>
        </SWRConfig>
      </RecoilRoot>
    </>
  );
}

const Wrap = ({ children }) => {
  return children;
};
