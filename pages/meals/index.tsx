import { MealForm } from "components/meals/form";
import { PageLoader, SavingLoader } from "components/shared/Loaders";
import { PageLayout } from "components/shared/PageLayout";
import { IList } from "lib/models/list.model";
import { endpoints } from "lib/router";
import Head from "next/head";
import useSWR, { mutate } from "swr";
import { useState } from "react";
import { MealPreview } from "components/lists/Preview";
import { Add } from "components/shared/Buttons";
import { MasonryLayout } from "components/layout/Masonry";
import { motion } from "framer-motion";
import { ListsPreviewPlaceholder } from "components/lists/ListsPreviewPlaceholder";

export default function Meals() {
  const { data: meals } = useSWR<IList[]>(endpoints.meals);
  const [addModalOpen, setAddModalOpen] = useState(false);

  const renderItem = (m, i) => {
    return (
      <div key={i} style={{ opacity: m.id ? 1 : 0.5 }}>
        <MealPreview meal={m} />
      </div>
    );
  };

  if (!meals?.length) {
    return (
      <div className="flex justify-center items-center space-x-1">
        <div className="max-w-xs">
          <p className="text-sm text-center leading-tight text-gray-400">
            Creating meals lets you easily apply the ingredients to one of you lists. Meals can also be added to create
            a dinner schedule
          </p>
        </div>
      </div>
    );
  }

  return (
    // <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }}>
    <>
      <Head>
        <title>Meals</title>
      </Head>

      <PageLayout>
        {meals ? <MasonryLayout items={meals} renderItem={renderItem} /> : <ListsPreviewPlaceholder />}
        <Add onClick={() => setAddModalOpen(true)} />
        <MealForm isOpen={addModalOpen} onClose={() => setAddModalOpen(false)} />
      </PageLayout>
    </>
  );
}
