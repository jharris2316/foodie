import { PageLayout } from "components/shared/PageLayout";
import Head from "next/head";
import { useRouter } from "next/router";
import { endpoints } from "lib/router";
import useSWR from "swr";
import { PageLoader } from "components/shared/Loaders";
import { withRouteIdParam } from "components/shared/withRouteParam";
import { MealDetail } from "components/meals/MealDetail";
import { motion } from "framer-motion";

export default withRouteIdParam(function Meal() {
  const { query } = useRouter();
  const { data: meal } = useSWR(`${endpoints.meals}/${query?.id}`);

  return (
    // <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }}>
    <>
      <Head>
        <title>Meal - {meal?.name}</title>
      </Head>

      <PageLayout>
        <MealDetail meal={meal} />
      </PageLayout>
    </>
  );
});
