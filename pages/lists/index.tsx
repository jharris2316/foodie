import { PageLoader } from "components/shared/Loaders";
import { PageLayout } from "components/shared/PageLayout";
import { IList } from "lib/models/list.model";
import { endpoints } from "lib/router";
import Head from "next/head";
import useSWR from "swr";
import { ListsLanding } from "components/lists/ListsLanding";
import { motion } from "framer-motion";

export default function Lists() {
  const listEndpoint = `${endpoints.lists}?type=list`;
  const { data: lists } = useSWR<IList[]>(listEndpoint);

  return (
    // <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }}>
    <>
      <Head>
        <title>Lists</title>
      </Head>

      <PageLayout>
        <ListsLanding lists={lists} type={"list"} endpoint={listEndpoint} />
      </PageLayout>
    </>
  );
}
