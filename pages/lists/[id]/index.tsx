import { PageLayout } from "components/shared/PageLayout";
import Head from "next/head";
import ListCrud from "components/lists/ListCrud";
import { useRouter } from "next/router";
import { withRouteIdParam } from "components/shared/withRouteParam";
import { endpoints } from "lib/router";
import useSWR from "swr";
import { PageLoader } from "components/shared/Loaders";
import { motion } from "framer-motion";

export default withRouteIdParam(function List(props) {
  const { query } = useRouter();

  const { data: list } = useSWR(`${endpoints.lists}/${query?.id}`);

  return (
    // <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }}>
    <>
      <Head>
        <title>List - {list?.name}</title>
      </Head>

      <PageLayout>
        <ListCrud list={list} type={"list"} />
      </PageLayout>
    </>
  );
});
