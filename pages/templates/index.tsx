import { PageLoader } from "components/shared/Loaders";
import { PageLayout } from "components/shared/PageLayout";
import { IList } from "lib/models/list.model";
import { endpoints } from "lib/router";
import Head from "next/head";
import useSWR from "swr";
import { ListsLanding } from "components/lists/ListsLanding";
import { motion } from "framer-motion";

export default function Templates() {
  const templatesEndpoint = `${endpoints.lists}?type=template`;
  const { data: lists } = useSWR<IList[]>(templatesEndpoint);

  return (
    // <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }}>
    <>
      <Head>
        <title>Templates</title>
      </Head>

      <PageLayout>
        <ListsLanding lists={lists} type={"template"} endpoint={templatesEndpoint} />
      </PageLayout>
    </>
  );
}
