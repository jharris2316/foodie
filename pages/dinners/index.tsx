import { PageLayout } from "components/shared/PageLayout";
import Head from "next/head";
import { WeekSelector } from "components/dinners/components/WeekSelector";
import { useState } from "react";
import { format, formatDistance, formatRelative, getWeek, eachWeekOfInterval, sub, add } from "date-fns";
import { WeekCalendar } from "components/dinners/components/WeekCalendar";
import { endpoints } from "lib/router";
import useSWR, { mutate, trigger } from "swr";
import { IDinner } from "lib/models/dinner.model";
import { arrayMutateCreator, arrayMutateDeleter } from "utils/swr-utils";
import { myFetch } from "lib/hooks/request";
import { nanoid } from "nanoid";
import { PageLoader } from "components/shared/Loaders";
import { motion } from "framer-motion";
import { WeekPlaceholder } from "components/dinners/components/WeekPlaceholder";

// import { format, formatDistance, formatRelative, subDays, eachWeekOfInterval, sub, add, getWeek, differenceInWeeks, formatDuration } from 'date-fns';

// // console.log(format(new Date()))

// const current = new Date();
// const beforeDate = sub(current, {
//   months: 3,
// });
// const endDate = add(current, {
//   months: 1,
// });

// const result = eachWeekOfInterval({
//   start: beforeDate,
//   end: endDate,
// });

// // console.log(result);

// const start = result.find(d => getWeek(d) === getWeek(current));

// const difference = differenceInWeeks(result[13], current)

// console.log(Math.floor(difference));

const current = new Date();
const beforeDate = sub(current, {
  months: 2,
});
const endDate = add(current, {
  months: 1,
});

const dateRange = eachWeekOfInterval({
  start: beforeDate,
  end: endDate,
});

const start = dateRange.findIndex((d) => getWeek(d) === getWeek(current));

export default function Dinners() {
  // grab dinner schedule
  const dinnersEndpoint = endpoints.dinners;
  const { data: dinners } = useSWR<IDinner[]>(dinnersEndpoint);
  const [week, setWeek] = useState(start);
  const onWeekChange = (index) => {
    setWeek(week + index);
    // console.log("week change", val);
  };

  const activeDate = dateRange[week];
  const onAdd = async ({ meal = null, day, isTakeout = false }) => {
    const justDate = format(day, "yyyy-MM-dd");
    try {
      const data = {
        id: `client-${nanoid()}`,
        dinnerAt: justDate,
        isTakeout,
        name: meal ? meal.name : "Takeout",
        mealId: meal?.id || null,
      };

      const adder = arrayMutateCreator(data, 0);

      mutate(dinnersEndpoint, adder, false);

      await myFetch<IDinner>(dinnersEndpoint, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      });

      trigger(dinnersEndpoint);
    } catch {}
  };

  const onDelete = async (dinner) => {
    const deleter = arrayMutateDeleter(dinner.id);
    mutate(dinnersEndpoint, deleter, false);

    await myFetch(`${dinnersEndpoint}/${dinner.id}`, {
      method: "DELETE",
    });
  };

  return (
    // <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }}>
    <>
      <Head>
        <title>Dinners</title>
      </Head>
      <PageLayout>
        <WeekSelector
          onWeekChange={onWeekChange}
          activeDate={activeDate}
          dateRange={dateRange}
          currentDate={current}
          week={week}
        />
        {dinners ? (
          <WeekCalendar week={activeDate} currentDate={current} dinners={dinners} onAdd={onAdd} onDelete={onDelete} />
        ) : (
          <WeekPlaceholder />
        )}
      </PageLayout>
    </>
  );
}
