import { LoginForm } from "components/ui/Login";
import { PageLayout } from "components/shared/PageLayout";
import { useIsLoggedIn } from "lib/hooks/user";
import { Routes } from "lib/router";
import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { Card } from "components/shared/Card";

export default function Login() {
  const router = useRouter();
  const isLoggedIn = useIsLoggedIn();

  // useEffect(() => {
  //   if (isLoggedIn) {
  //     router.push(Routes.Home);
  //   }
  // }, []);

  return (
    <>
      <Head>
        <title>Login</title>
      </Head>
      <div className="h-screen flex flex-col justify-center">
        <PageLayout>
          <div className="max-w-sm mx-auto -mt-16">
            <Card>
              <Card.Body>
                <LoginForm />
              </Card.Body>
            </Card>
          </div>
        </PageLayout>
      </div>
    </>
  );
}
