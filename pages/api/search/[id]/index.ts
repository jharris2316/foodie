import { deleteRecord } from "lib/algolia";
import { authMiddleware, validationMiddleware } from "utils/middlewares";
import { createHandlers } from "utils/rest";

const searchValidator = validationMiddleware({
  id: "required",
});

const handlers = {
  DELETE: searchValidator(async (req, res) => {
    const {
      query: { id: index },
    } = req;

    try {
      await deleteRecord(index, req.sanitizedData?.id);

      return res.status(200).json({});
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  }),
};

export default async function handler(req, res) {
  const handler = authMiddleware(createHandlers(handlers));
  return handler(req, res);
}
