import { getSession } from "utils/auth-cookies";
import { createHandlers } from "utils/rest";

const handlers = {
  GET: async (req, res) => {
    try {
      const session = await getSession(req);
      if (session) {
        const { user } = session;
        res.status(200).json({ user });
      } else {
        res.status(200).json({ user: null });
      }
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  },
};

export default async function handler(req, res) {
  const handler = createHandlers(handlers);
  return handler(req, res);
}
