import { IList, ListModel } from "lib/models/list.model";
import { createHandlers } from "utils/rest";
import { authMiddleware, validationMiddleware } from "utils/middlewares";
import { GroupModel } from "lib/models/group.model";

const listValidator = validationMiddleware({
  name: "required",
  group: "required",
  type: "required",
});

const handlers = {
  GET: async (req, res) => {
    try {
      const model = new ListModel(req.token);

      const type = req?.query?.type || "list";

      const groupModel = new GroupModel();
      const groupRef = await groupModel.getRef(req.currentUser?.group);

      const lists = await model.getLists(type, groupRef);

      return res.status(200).json(lists);
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  },
  POST: listValidator(async (req, res) => {
    try {
      const listModel = new ListModel(req?.token);

      const { name, type } = req.sanitizedData;

      const groupModel = new GroupModel();
      const groupRef = await groupModel.getRef(req.currentUser?.group);

      const data = {
        name,
        userRef: req.faunaUserRef,
        groupRef,
        type,
      };

      const newList = await listModel.create<IList>(data);

      return res.status(200).json(newList);
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  }),
};

export default async function handler(req, res) {
  const handler = authMiddleware(createHandlers(handlers));
  return handler(req, res);
}
