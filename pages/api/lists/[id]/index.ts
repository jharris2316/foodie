import { q } from "lib/faunadb";
import { IList, ListModel } from "lib/models/list.model";
import { authMiddleware, validationMiddleware } from "utils/middlewares";
import { createHandlers } from "utils/rest";

const listValidator = validationMiddleware({});

const handlers = {
  GET: async (req, res) => {
    const {
      query: { id },
    } = req;

    try {
      const model = new ListModel(req.token);
      const data = await model.getById<IList>(id);
      return res.status(200).json(data);
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  },
  PUT: listValidator(async (req, res) => {
    const {
      query: { id },
    } = req;

    try {
      const model = new ListModel(req.token);
      const updatedList = await model.update<IList>(id, {
        ...(req.sanitizedData || {}),
      });

      return res.status(200).json(updatedList);
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  }),
  DELETE: async (req, res) => {
    const {
      query: { id },
    } = req;

    try {
      const model = new ListModel(req.token);
      await model.delete(id);

      return res.status(200).json({});
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  },
};

export default async function handler(req, res) {
  const handler = authMiddleware(createHandlers(handlers));
  return handler(req, res);
}
