import { q } from "lib/faunadb";
import { IItem, ItemModel } from "lib/models/item.model";
import { authMiddleware, validationMiddleware } from "utils/middlewares";
import { createHandlers } from "utils/rest";

const itemValidator = validationMiddleware({
  completed: "required",
});

const handlers = {
  GET: async (req, res) => {
    const {
      query: { id },
    } = req;

    const limit = +req?.query?.limit || 100;
    const completed = req?.query?.completed ? true : false;

    try {
      const model = new ItemModel(req.token);
      const data = await model.getItemsByListId<IItem[]>(id, completed, { limit });
      return res.status(200).json(data);
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  },
  DELETE: itemValidator(async (req, res) => {
    const {
      query: { id },
    } = req;

    const completed = req.sanitizedData?.completed || false;

    try {
      const model = new ItemModel(req.token);
      await model.deleteItemsByListId(id, completed);
      return res.status(200).json({});
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  }),
};

export default async function handler(req, res) {
  const handler = authMiddleware(createHandlers(handlers));
  return handler(req, res);
}
