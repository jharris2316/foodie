import { q } from "lib/faunadb";
import { IItem, ItemModel } from "lib/models/item.model";
import { ListModel } from "lib/models/list.model";
import { authMiddleware, validationMiddleware } from "utils/middlewares";
import { createHandlers } from "utils/rest";

const listValidator = validationMiddleware({
  destinationListId: "required",
});

const handlers = {
  POST: listValidator(async (req, res) => {
    const {
      query: { id },
    } = req;

    const { destinationListId } = req.sanitizedData;

    try {
      const model = new ItemModel(req.token);
      const listModel = new ListModel(req.token);
      const data = await model.getItemsByListId<IItem[]>(id);
      const listRef = await listModel.getRef(destinationListId);

      if (!listRef) {
        return res.status(422).send("List not found");
      }

      if (!data.length) {
        return res.status(200).json({});
      }

      const newItems: any = data.map((item) => ({
        ...item,
        listRef,
      }));

      const promises = [];

      newItems.forEach((item) => {
        promises.push(model.create<IItem>(item));
      });

      await Promise.all(promises);

      return res.status(200).json({});
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  }),
};

export default async function handler(req, res) {
  const handler = authMiddleware(createHandlers(handlers));
  return handler(req, res);
}
