import { updateRecord, deleteRecord, addRecord } from "lib/algolia";
import { q } from "lib/faunadb";
import { IItem, ItemModel } from "lib/models/item.model";
import { authMiddleware, validationMiddleware } from "utils/middlewares";
import { createHandlers } from "utils/rest";

const itemValidator = validationMiddleware({});

const handlers = {
  GET: async (req, res) => {
    const {
      query: { id },
    } = req;

    try {
      const itemModel = new ItemModel(req.token);
      const data = await itemModel.getById<IItem>(id);
      return res.status(200).json(data);
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  },
  PUT: itemValidator(async (req, res) => {
    const {
      query: { id },
    } = req;

    try {
      const data = {
        ...(req.sanitizedData || {}),
      };

      const itemModel = new ItemModel(req.token);
      const updatedItem = await itemModel.update<IItem>(id, {
        ...data,
      });

      const { id: updatedId, order: newOrder, completed, ...updateItemRecord } = updatedItem;

      if (updatedItem.name?.length > 2) {
        addRecord(
          "Item",
          {
            id: updatedItem.name,
            itemId: id,
            ...updateItemRecord,
          },
          req.currentUser,
        );
      }

      return res.status(200).json(updatedItem);
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  }),
  DELETE: async (req, res) => {
    const {
      query: { id },
    } = req;

    try {
      const itemModel = new ItemModel(req.token);
      await itemModel.delete(id);

      return res.status(200).json({});
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  },
};

export default async function handler(req, res) {
  const handler = authMiddleware(createHandlers(handlers));
  return handler(req, res);
}
