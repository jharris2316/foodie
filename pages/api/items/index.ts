import { ListModel } from "lib/models/list.model";
import { createHandlers } from "utils/rest";
import { authMiddleware, validationMiddleware } from "utils/middlewares";
import { GroupModel } from "lib/models/group.model";
import { IItem, ItemCategory, ItemModel } from "lib/models/item.model";
import { addRecord } from "lib/algolia";

const itemValidator = validationMiddleware({
  group: "required",
  order: "required",
  listId: "required",
});

const handlers = {
  POST: itemValidator(async (req, res) => {
    try {
      const itemModel = new ItemModel(req?.token);
      const listModel = new ListModel(req?.token);
      const groupModel = new GroupModel();

      const { name, group, order, listId, category = "" } = req.sanitizedData;
      const groupRef = await groupModel.getRef(group);
      const listRef = await listModel.getRef(listId);

      const data = {
        name,
        order,
        completed: false,
        userRef: req.faunaUserRef,
        groupRef,
        listRef,
        category,
      };
      const newItem = await itemModel.create<IItem>(data);

      const { id, order: newOrder, completed, ...newItemRecord } = newItem;

      // dont bother adding to autocomplete if it
      if (name?.length > 2) {
        addRecord(
          "Item",
          {
            id: newItemRecord.name,
            itemId: id,
            ...newItemRecord,
          },
          req.currentUser,
        );
      }

      return res.status(200).json(newItem);
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  }),
};

export default async function handler(req, res) {
  const handler = authMiddleware(createHandlers(handlers));
  return handler(req, res);
}
