import { createHandlers } from "utils/rest";
import { authMiddleware, validationMiddleware } from "utils/middlewares";
import { q } from "lib/faunadb";
import { IItem, ItemModel } from "lib/models/item.model";

const { Now } = q;

const reorderValidator = validationMiddleware({
  reorderedIds: "array",
});

const handlers = {
  PUT: reorderValidator(async (req, res) => {
    try {
      const { reorderedIds } = req.sanitizedData;

      const promises = [];
      const itemModel = new ItemModel(req.token);

      reorderedIds.forEach(async (id, i) => {
        promises.push(
          itemModel.update<IItem>(id, {
            order: i,
          }),
        );
      });

      await Promise.all(promises);

      return res.status(200).json({});
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  }),
};

export default async function handler(req, res) {
  const handler = authMiddleware(createHandlers(handlers));
  return handler(req, res);
}
