import { q, serverClient } from "lib/faunadb";
import { UserModel } from "lib/models/user.model";
import { createSession } from "utils/auth-cookies";
import { createHandlers } from "utils/rest";

const handlers = {
  POST: async (req, res) => {
    const { email, password } = req.body;

    if (!email || !password) {
      return res.status(400).send("Email and Password not provided");
    }

    try {
      const auth = await serverClient.query<any>(
        q.Login(q.Match(q.Index("users_by_email"), q.Casefold(email)), {
          password,
        }),
      );

      if (!auth.secret) {
        return res.status(404).send("auth secret is missing");
      }

      const model = new UserModel();
      const user = await model.getUserByEmail(email);

      await createSession(res, { token: auth.secret, user });

      res.status(200).json(user);
    } catch (error) {
      console.error(error);
      res.status(error.requestResult.statusCode).send(error.message);
    }
  },
};

export default async function handler(req, res) {
  const handler = createHandlers(handlers);
  return handler(req, res);
}
