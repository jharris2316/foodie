import { IList, ListModel } from "lib/models/list.model";
import { createHandlers } from "utils/rest";
import { authMiddleware, validationMiddleware } from "utils/middlewares";
import { GroupModel } from "lib/models/group.model";
import { q } from "lib/faunadb";
import { IMeal, MealModel } from "lib/models/meal.model";

const { Now } = q;

const mealValidator = validationMiddleware({
  name: "required",
});

const handlers = {
  GET: async (req, res) => {
    try {
      const model = new MealModel(req.token);
      const groupModel = new GroupModel();
      const groupRef = await groupModel.getRef(req.currentUser?.group);

      const meals = await model.get(groupRef);
      return res.status(200).json(meals);
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  },
  POST: mealValidator(async (req, res) => {
    try {
      const model = new MealModel(req.token);

      const { name, description = "" } = req.sanitizedData;

      const groupModel = new GroupModel();
      const groupRef = await groupModel.getRef(req.currentUser?.group);

      // create corresonding list
      const list = {
        name: `${name} items`,
        userRef: req.faunaUserRef,
        groupRef,
        type: "meal" as const,
      };

      const listModel = new ListModel(req.token);
      const newList = await listModel.create<IList>(list);

      const data = {
        name,
        description,
        userRef: req.faunaUserRef,
        groupRef,
        listRef: newList.ref,
      };

      const newMeal = await model.create<IMeal>(data);

      return res.status(200).json(newMeal);
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  }),
};

export default async function handler(req, res) {
  const handler = authMiddleware(createHandlers(handlers));
  return handler(req, res);
}
