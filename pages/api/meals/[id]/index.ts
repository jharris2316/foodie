import { IMeal, MealModel } from "lib/models/meal.model";
import { authMiddleware, validationMiddleware } from "utils/middlewares";
import { createHandlers } from "utils/rest";

const mealValidator = validationMiddleware({});

const handlers = {
  GET: async (req, res) => {
    const {
      query: { id },
    } = req;

    try {
      const model = new MealModel(req.token);
      const data = await model.getById(id);
      return res.status(200).json(data);
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  },
  PUT: mealValidator(async (req, res) => {
    const {
      query: { id },
    } = req;

    try {
      const model = new MealModel(req.token);
      const updateMeal = await model.update<IMeal>(id, {
        ...(req.sanitizedData || {}),
      });

      return res.status(200).json(updateMeal);
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  }),
  DELETE: async (req, res) => {
    const {
      query: { id },
    } = req;

    try {
      const model = new MealModel(req.token);
      await model.delete(id);

      return res.status(200).json({});
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  },
};

export default async function handler(req, res) {
  const handler = authMiddleware(createHandlers(handlers));
  return handler(req, res);
}
