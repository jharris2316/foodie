import { createHandlers } from "utils/rest";
import { authMiddleware, validationMiddleware } from "utils/middlewares";
import { GroupModel } from "lib/models/group.model";
import { q } from "lib/faunadb";
import { IMeal, MealModel } from "lib/models/meal.model";
import { DinnerModel, IDinner } from "lib/models/dinner.model";
import { add, sub, format } from "date-fns";

const { Time, Date: FaundaDate } = q;

const dinnerValidator = validationMiddleware({
  dinnerAt: "required",
  name: "required",
});

const handlers = {
  GET: async (req, res) => {
    try {
      const model = new DinnerModel(req.token);
      const groupModel = new GroupModel();
      const groupRef = await groupModel.getRef(req.currentUser?.group);

      const current = new Date();
      const defaultStartAt = format(
        sub(current, {
          months: 2,
        }),
        "yyyy-MM-dd",
      );
      const defaultEndAt = format(
        add(current, {
          months: 1,
        }),
        "yyyy-MM-dd",
      );

      const { startAt = defaultStartAt, endAt = defaultEndAt } = req.query;

      const dinners = await model.getDinnersByRange(groupRef, startAt, endAt);
      return res.status(200).json(dinners);
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  },
  POST: dinnerValidator(async (req, res) => {
    try {
      const model = new DinnerModel(req.token);
      const groupModel = new GroupModel();
      const mealModel = new MealModel(req.token);
      const groupRef = await groupModel.getRef(req.currentUser?.group);

      const { dinnerAt, isTakeout = false, mealId = null, name } = req.sanitizedData;

      const data = {
        dinnerAt: FaundaDate(dinnerAt),
        name,
        isTakeout,
        mealRef: null,
        groupRef,
        userRef: req.faunaUserRef,
      };

      if (mealId) {
        const meal = await mealModel.getById<IMeal>(mealId);

        if (!meal) {
          const error = new Error("Meal not found");
          (error as any).status = 422;
          throw error;
        }

        data.mealRef = meal.ref;
      }

      const newDinner = await model.create<IDinner>(data);

      return res.status(200).json(newDinner);
    } catch (e) {
      console.error(e);
      res.status(e?.status || e.requestResult.statusCode).send(e.message);
    }
  }),
};

export default async function handler(req, res) {
  const handler = authMiddleware(createHandlers(handlers));
  return handler(req, res);
}
