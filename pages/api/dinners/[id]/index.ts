import { DinnerModel } from "lib/models/dinner.model";
import { authMiddleware } from "utils/middlewares";
import { createHandlers } from "utils/rest";

const handlers = {
  DELETE: async (req, res) => {
    const {
      query: { id },
    } = req;

    try {
      const model = new DinnerModel(req.token);
      await model.delete(id);

      return res.status(200).json({});
    } catch (e) {
      console.error(e);
      res.status(e.requestResult.statusCode).send(e.message);
    }
  },
};

export default async function handler(req, res) {
  const handler = authMiddleware(createHandlers(handlers));
  return handler(req, res);
}
