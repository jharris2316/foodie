import { getClient, q } from "lib/faunadb";
import { removeSession } from "utils/auth-cookies";
import { authMiddleware } from "utils/middlewares";
import { createHandlers } from "utils/rest";

const handlers = {
  GET: async (req, res) => {
    try {
      await getClient(req?.token).query(q.Logout(true));

      removeSession(res);

      res.status(200).json({});
    } catch (error) {
      console.error(error);
      res.status(error.requestResult.statusCode).send(error.message);
    }
  },
};

export default function logout(req, res) {
  const handler = authMiddleware(createHandlers(handlers));
  return handler(req, res);
}
