import React, { FC } from "react";
import { colors } from "lib/style-config";

export const PageLoader = () => {
  return (
    <div className="py-10">
      <Loader />
    </div>
  );
};

export const SavingLoader = ({ position = "fixed" }) => {
  return (
    <div className={`${position} bottom-2 right-2 z-40`}>
      <Spinner size={28} color={colors.primary.DEFAULT} />
    </div>
  );
};

export const Loader = ({ size = 40 }) => {
  return (
    <div className="text-center">
      <div className="spinner" style={{ width: size, height: size, margin: "0 auto" }}>
        <div className="bg-indigo-700 double-bounce1"></div>
        <div className="bg-indigo-700 double-bounce2"></div>
      </div>
    </div>
  );
};

export const Spinner = ({ size = 25, color = "#000000" }) => (
  <>
    <div className="loader"></div>
    <style jsx>{`
      .loader,
      .loader:after {
        border-radius: 50%;
        width: ${size}px;
        height: ${size}px;
      }
      .loader {
        position: relative;
        border-top: ${size / 6}px solid rgba(255, 255, 255, 0.3);
        border-right: ${size / 6}px solid rgba(255, 255, 255, 0.3);
        border-bottom: ${size / 6}px solid rgba(255, 255, 255, 0.3);
        border-left: ${size / 6}px solid ${color};
        -webkit-transform: translateZ(0);
        -ms-transform: translateZ(0);
        transform: translateZ(0);
        -webkit-animation: load8 1.1s infinite linear;
        animation: load8 1.1s infinite linear;
      }
      @-webkit-keyframes load8 {
        0% {
          -webkit-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }
      @keyframes load8 {
        0% {
          -webkit-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }
    `}</style>
  </>
);
