type Variant = "primary" | "secondary" | "text";
type Sizes = "sm" | "md" | "lg" | "xl";
import Portal from "@reach/portal";
import classNames from "classnames/bind";
import { Plus } from "heroicons-react";

export const Button = ({ children, ...props }) => {
  const Component = props.href ? AComp : ButtonComp;
  const variant = props?.variant || "primary";
  const size = props?.size || "md";
  const active = props?.active || false;

  return (
    <Component {...props} variant={variant} size={size} active={active}>
      {children}
    </Component>
  );
};

const sharedStyles = "inline-block rounded-full transition-all outline-none focus:outline-none shadow-md";
const btnStyles = {
  primary: {
    default: "bg-primary hover:bg-primary-light text-white active:bg-primary-darker",
    active: "bg-gray-700 text-white shadow-none",
  },
  secondary: {
    default: "bg-secondary hover:bg-secondary-light text-white ",
    active: "bg-secondary hover:bg-secondary-light text-white ",
  },
  text: {
    default: "text-secondary hover:text-secondary-light text-sm hover:underline shadow-none",
    active: "text-secondary hover:text-secondary-light shadow-none",
  },
  icon: {
    default: "text-white bg-primary-light bg-primary",
    active: "bg-primary hover:text-primary-light",
  },
};

const btnSizes = {
  sm: "py-1 px-4 text-sm",
  md: "py-1 px-6",
  lg: "py-1 px-7 text-lg",
  xl: "py-2 px-8 text-xl",
  icon: "p-2",
};

const AComp = ({ children, variant, size, active, ...props }) => (
  <a
    {...props}
    className={classNames(
      sharedStyles,
      btnStyles?.[variant]?.[active ? "active" : "default"] || "",
      btnSizes[variant === "icon" ? "icon" : size] || "",
    )}>
    {children}
  </a>
);
const ButtonComp = ({ children, variant, size, active, ...props }) => (
  <button
    {...props}
    className={classNames(
      sharedStyles,
      btnStyles?.[variant]?.[active ? "active" : "default"] || "",
      btnSizes[variant === "icon" ? "icon" : size] || "",
    )}>
    {children}
  </button>
);

export const Add = ({ onClick, disabled = false }) => (
  <Portal>
    <div className="fixed bottom-5 left-0 right-0">
      <div className="container mx-auto px-4">
        <div className="max-w-2xl mx-auto flex justify-end">
          <button
            onClick={onClick}
            className={classNames("transition", {
              "text-primary hover:text-primary-light": !disabled,
              "text-gray-600 cursor-default": disabled,
            })}>
            <Plus size={50} />
          </button>
        </div>
      </div>
    </div>
  </Portal>
);
