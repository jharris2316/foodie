import { atom } from "recoil";

export interface ISearchState {
  deletedHits: string[];
}

export const initialSearchState: ISearchState = {
  deletedHits: [],
};

export const searchState = atom<ISearchState>({
  key: "search",
  default: { ...initialSearchState },
});
