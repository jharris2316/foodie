// Import components
import { InstantSearch } from "react-instantsearch-dom";
import { connectAutoComplete } from "react-instantsearch-dom";
import { Input } from "../Form";
import algolia from "algoliasearch/lite";
import { FC, useRef, useState } from "react";
import { X } from "heroicons-react";
import { endpoints } from "lib/router";
import { myFetch } from "lib/hooks/request";
import { useRecoilState } from "recoil";
import { searchState } from "./state";

const client = algolia(process.env.ALGOLIA_APPLICATION_ID, process.env.ALGOLIA_PUBLIC_KEY);

export const Search = ({ index, children }) => {
  return (
    <InstantSearch indexName={`prod_${index}`} searchClient={client}>
      {children}
    </InstantSearch>
  );
};

const fallback = (val) => null;

const AutocompleteWrap: FC<any> = ({
  hits,
  refine,
  onChange: onChangeProp = fallback,
  onSelect: onSelectProp = fallback,
  onSubmit: onSubmitProp = fallback,
  onBlur: onBlurProp = fallback,
  defaultValue = "",
  resetOnSelect = true,
  index,
  ...rest
}) => {
  const [value, setValue] = useState(defaultValue);
  const [visible, setVisible] = useState(false);
  const elRef = useRef<HTMLInputElement>(null);
  const [searchStateValues, setSearchState] = useRecoilState(searchState);

  const onBlur = (e) => {
    hideDropdown();
    onBlurProp(e);
  };

  const onSelect = (item) => {
    onSelectProp(item);
    setVisible(false);
    setValue(item.name);
    reset();
  };

  const onChange = (e) => {
    const value = e.target.value;
    setVisible(true);
    setValue(value);
    refine(value);
    onChangeProp({ name: value });
  };

  const onSubmit = (e) => {
    e.preventDefault(e);

    const formData = new FormData(e.target);
    const name = formData.get("item");

    onSubmitProp({ name });
    reset();
  };

  const reset = () => {
    if (resetOnSelect) {
      setValue("");
      if (elRef.current) {
        elRef.current.focus();
      }
    }

    hideDropdown();
  };

  const hideDropdown = () => {
    setTimeout(() => {
      setVisible(false);
    }, 300);
  };

  const deleteHit = async (item) => {
    if (elRef.current) {
      elRef.current.focus();
    }

    setSearchState((s) => ({ ...s, deletedHits: [...s.deletedHits, item.id] }));

    await myFetch(`${endpoints.searchIndex(index)}`, {
      method: "DELETE",
      body: JSON.stringify({
        id: item.id,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });
  };

  const filteredHits = hits.filter((h) => !searchStateValues.deletedHits.includes(h.id)).slice(0, 4);

  return (
    <form onSubmit={onSubmit} autoComplete="off" className="block w-full">
      <div className="relative w-full">
        {filteredHits?.length > 0 && value?.length > 1 && visible ? (
          <div className="overflow-y-auto absolute bottom-full max-h-44 left-0 w-52 mb-1 bg-gray-700 shadow-xl rounded-lg overflow-hidden border border-gray-600">
            {filteredHits.map((h) => (
              <div className="hover:bg-gray-600 px-4 relative" key={h.objectID}>
                <span className="text-sm py-2 block mr-5 cursor-pointer " onClick={() => onSelect(h)}>
                  {h.name}
                </span>
                <button
                  type="button"
                  onClick={() => deleteHit(h)}
                  className="absolute right-1 top-1/2 transform -translate-y-1/2 text-gray-500 hover:text-gray-300">
                  <X size={14} />
                </button>
              </div>
            ))}
          </div>
        ) : null}

        <Input
          onBlur={onBlur}
          // onFocus={onFocus}
          onChange={onChange}
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="on"
          spellCheck="false"
          name="item"
          value={value}
          ref={elRef}
          {...rest}
        />
      </div>
    </form>
  );
};

const ConnectedAutocomplete = connectAutoComplete(AutocompleteWrap);

export const Autocomplete = ({ index, ...props }) => {
  return (
    <Search index={index}>
      <ConnectedAutocomplete {...props} index={index} />
    </Search>
  );
};
