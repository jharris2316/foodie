import { Menu } from "@headlessui/react";
import classNames from "classnames/bind";

export const DropdownItems = ({ children }) => (
  <Menu.Items className="absolute bg-gray-800 border border-gray-700 divide-gray-700 divide-y rounded-md shadow-lg w-40 focus:outline-none ring-0 right-1 top-10 z-10">
    {children}
  </Menu.Items>
);

export const DropdownItem = ({ children, condensed = false }) => (
  <div
    className={classNames("px-4 text-xs cursor-pointer hover:bg-gray-700", {
      "py-3": !condensed,
      "py-1.5": condensed,
    })}>
    {children}
  </div>
);
