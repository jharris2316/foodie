import Link from "next/link";
import { useRouter } from "next/router";
import { useIsLoggedIn } from "lib/hooks/user";
import { Routes } from "lib/router";
import { Button } from "./Buttons";
import { Home } from "heroicons-react";

export const Nav = () => {
  const loggedIn = useIsLoggedIn();

  return loggedIn ? <LoggedInNav /> : <LoggedOutNav />;
};

const LoggedOutNav = () => {
  return null;
};
const LoggedInNav = () => {
  const router = useRouter();

  return (
    <nav className="py-4">
      <div className="flex items-center justify-center space-x-1">
        {/* <div>
          <Link href={Routes.Home}>
            <a>
              <Button size="sm" active={router.pathname === Routes.Home}>
                Home
              </Button>
            </a>
          </Link>
        </div> */}
        <div>
          <Link href={Routes.Lists}>
            <a>
              <Button size="sm" active={router.pathname.includes(Routes.Lists)}>
                Lists
              </Button>
            </a>
          </Link>
        </div>
        <div>
          <Link href={Routes.Templates}>
            <a>
              <Button size="sm" active={router.pathname.includes(Routes.Templates)}>
                Templates
              </Button>
            </a>
          </Link>
        </div>
        <div>
          <Link href={Routes.Meals}>
            <a>
              <Button size="sm" active={router.pathname.includes(Routes.Meals)}>
                Meals
              </Button>
            </a>
          </Link>
        </div>
        <div>
          <Link href={Routes.Dinners}>
            <a>
              <Button size="sm" active={router.pathname.includes(Routes.Dinners)}>
                Dinners
              </Button>
            </a>
          </Link>
        </div>
        {/* <li className="mx-2">
          <Link href="/presets">
            <Button size="sm">Presets</Button>
          </Link>
        </li>
        <li className="mx-2">
          <Link href="/meals">
            <Button size="sm">Meals</Button>
          </Link>
        </li> */}
        {/* <li className="mx-2">
          <span onClick={logout} className="cursor-pointer">
            Logout
          </span>
        </li> */}
      </div>
    </nav>
  );
};
