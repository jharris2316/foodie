import { useRouter } from "next/router";

export const withRouteIdParam = (Component) => (props) => {
  const router = useRouter();
  if (!router.query?.id) return null;
  return <Component {...props} />;
};
