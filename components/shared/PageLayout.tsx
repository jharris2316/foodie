export const PageLayout = ({ children }) => {
  return (
    <main className="main">
      <div className="container mx-auto px-4">
        <div className="max-w-2xl mx-auto relative">{children}</div>
      </div>
    </main>
  );
};
