import { FC, forwardRef, memo, useState } from "react";
import { CustomCheckbox, CustomCheckboxContainer, CustomCheckboxInput } from "@reach/checkbox";
import "@reach/checkbox/styles.css";
import { Check } from "heroicons-react";
import classNames from "classnames/bind";

export const Input = forwardRef<HTMLInputElement, any>((props, ref) => {
  return (
    <input
      {...props}
      ref={ref}
      className="focus:shadow-none ring-transparent focus:ring-transparent border-0 border-b py-2 px-3 block w-full focus:border-gray-500 outline-none border-gray-700 transition duration-300 bg-transparent"
    />
  );
});
export const Checkbox = forwardRef<HTMLInputElement, any>((props, ref) => {
  return (
    <input
      {...props}
      type="checkbox"
      ref={ref}
      className="form-checkbox h-5 w-5 rounded text-secondary-dark border-secondary-dark bg-gray-800"
    />
  );
});

export const FieldInput = ({ children }) => <div className="mb-2 relative">{children}</div>;

export const Label = ({ children }) => <label className="block w-full mb-1">{children}</label>;

export const FieldError = ({ children }) => <div className="text-red-600 text-sm">{children}</div>;

export const FormActions = ({ children }) => <div className="flex justify-center items-center my-4">{children}</div>;

export const Textarea = forwardRef<HTMLTextAreaElement, any>((props, ref) => (
  <textarea
    ref={ref}
    {...props}
    className="h-28 focus:shadow-none ring-transparent focus:ring-transparent border-0 border-b py-2 px-3 block w-full focus:border-gray-500 outline-none border-gray-700 transition duration-300 bg-transparent"></textarea>
));
