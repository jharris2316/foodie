import { atom } from "recoil";

export interface IToastOptions {
  type: "default" | "error" | "success";
  autoDismiss: boolean;
  timer: number;
}

export interface IToastState {
  isOpen: boolean;
  message: string;
  options: IToastOptions;
}

export const initialToastState: IToastState = {
  isOpen: false,
  message: "",
  options: {
    type: "default",
    autoDismiss: true,
    timer: 5000,
  },
};

export const toastState = atom<IToastState>({
  key: "toast",
  default: { ...initialToastState },
});
