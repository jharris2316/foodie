import Portal from "@reach/portal";
import { X } from "heroicons-react";
import { useCallback, useEffect, useRef } from "react";
import { useRecoilValue, useResetRecoilState, useSetRecoilState } from "recoil";
import { IToastOptions, toastState } from "./state";
import classNames from "classnames/bind";

const defaultOptions: IToastOptions = {
  type: "default",
  autoDismiss: true,
  timer: 5000,
};

export const useToast = (options?: Partial<IToastOptions>) => {
  const setToastState = useSetRecoilState(toastState);

  const show = useCallback(
    (message) => {
      setToastState({
        isOpen: true,
        message,
        options: {
          ...defaultOptions,
          ...options,
        },
      });
    },
    [options],
  );

  return {
    show,
  };
};

export const Toast = () => {
  const { isOpen, message, options } = useRecoilValue(toastState);
  const resetState = useResetRecoilState(toastState);
  const timer = useRef(null);

  useEffect(() => {
    if (isOpen) {
      if (options.autoDismiss) {
        timer.current = setTimeout(onClose, options.timer);
      } else {
        if (timer.current) {
          clearTimeout(timer.current);
        }
      }
    }
  }, [isOpen, options.autoDismiss, options.timer]);

  const onClose = () => {
    timer.current = null;
    resetState();
  };

  return (
    <Portal>
      <div
        className={classNames(
          "fixed bottom-0 left-0 right-0 shadow transition duration-300 overflow-hidden transform translate-y-0",
          {
            "translate-y-full opacity-0": !isOpen,
            "bg-primary": options.type === "default",
            "bg-red-800": options.type === "error",
          },
        )}>
        <div className={classNames("absolute top-1 right-1")}>
          {!options.autoDismiss && (
            <button onClick={onClose} type="button" className="cursor-pointer">
              <X size={18} />
            </button>
          )}
        </div>
        <div className="max-w-2xl w-11/12 mx-auto py-3 px-4 text-center">
          <p className="text-sm">{message}</p>
        </div>
      </div>
    </Portal>
  );
};

// type: "error" | "default"; -> default
// autoDismiss: boolean; -> true
// timer: number; 5000ms

// const toast = useToast({});

// toast.show("Message")
// toast.hide();
