import { ArrowLeft } from "heroicons-react";
import { useRouter } from "next/router";

export const Card = ({ children }) => (
  <div className="bg-gray-800 rounded-lg shadow-sm border border-gray-700 relative">{children}</div>
);

const Header = ({ children }) => {
  return (
    <div className="bg-gray-700 border-b border-gray-600 flex items-center justify-between py-2 px-2 relative rounded-t-lg">
      {children}
    </div>
  );
};

const Body = ({ children }) => {
  return <div className="p-3">{children}</div>;
};

const InnerPageHeader = ({ backTitle, children }) => {
  const { back } = useRouter();
  return (
    <Card.Header>
      <div>
        <button onClick={back}>
          <a className="inline-block p-2 hover:text-primary">
            <div className="flex items-center space-x-2">
              <ArrowLeft size={18} />
              <h4 className="capitalize">{backTitle}</h4>
            </div>
          </a>
        </button>
      </div>
      <div>{children}</div>
    </Card.Header>
  );
};

Card.Header = Header;
Card.Body = Body;
Card.InnerPageHeader = InnerPageHeader;
