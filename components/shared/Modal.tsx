import Dialog from "@reach/dialog";
import "@reach/dialog/styles.css";
import { X } from "heroicons-react";

export const Modal = ({ isOpen, onClose, children }) => {
  return (
    <Dialog
      style={{
        padding: 0,
        width: "95vw",
        maxWidth: 600,
        zIndex: 50,
        backgroundColor: "#262626",
      }}
      isOpen={isOpen}
      onDismiss={onClose}
      aria-label="list-modal-selector"
      className="rounded-lg bg-gray-700">
      {children}
    </Dialog>
  );
};

const Header = ({ children, onClose = () => null }) => (
  <div className="relative flex items-center justify-between py-2 pl-4 pr-2 bg-gray-700 border-b border-gray-600 rounded-t-lg">
    {children}
    {onClose && (
      <button onClick={onClose} className=" text-paragraph hover:text-paragraph-light">
        <X />
      </button>
    )}
  </div>
);

const Body = ({ children }) => <div className="relative px-4 py-4 pb-8 min-w-4/5">{children}</div>;
const Title = ({ children }) => <h3>{children}</h3>;

Modal.Header = Header;
Modal.Body = Body;
Modal.Title = Title;
