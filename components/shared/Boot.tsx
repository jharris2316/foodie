import React, { useEffect } from "react";
import { sessionState } from "lib/auth/state";
import { useRecoilState } from "recoil";
import { Loader } from "components/shared/Loaders";
import { useUser } from "lib/hooks/user";

/**
 * if we haven't loaded in 10 seconds lets just send them to login
 * Something probably went wrong
 */
const KILLSWITCH = 10000;

export const Boot = ({ children }) => {
  const [sessionStore, setSessionState] = useRecoilState(sessionState);
  const { user, loading: userLoading } = useUser();

  useEffect(() => {
    const init = async () => {
      if (!userLoading) {
        setSessionState({
          user,
          resolved: true,
        });
      }

      // setTimeout(() => {
      //   setSessionState((s) => ({ ...s, resolved: true }));
      //   router.push(Routes.Login);
      // }, KILLSWITCH);
    };

    init();
  }, [user, userLoading, setSessionState]);

  return (
    <>
      {sessionStore.resolved ? (
        children
      ) : (
        <div className="h-screen flex flex-col justify-center items-center">
          <Loader size={60} />
        </div>
      )}
    </>
  );
};
