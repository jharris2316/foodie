import { Nav } from "./Nav";
import Link from "next/link";
import { Menu } from "@headlessui/react";
import { DotsHorizontal, User } from "heroicons-react";
import { DropdownItem, DropdownItems } from "./Dropdown";
import { useRouter } from "next/router";
import { initialSessionState, sessionState } from "lib/auth/state";
import { useSetRecoilState } from "recoil";
import { myFetch } from "lib/hooks/request";
import { endpoints, Routes } from "lib/router";
import { useIsLoggedIn } from "lib/hooks/user";
import Image from "next/image";

export const Header = () => {
  const router = useRouter();
  const setSessionState = useSetRecoilState(sessionState);
  const loggedIn = useIsLoggedIn();

  const logout = async () => {
    try {
      await myFetch(endpoints.logout);

      setSessionState({
        ...initialSessionState,
        resolved: true,
      });
      router.push(Routes.Login);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className="container mx-auto py-1 relative px-2">
      <div className="w-full h-16 bg-gray-900 z-40 flex items-center">
        <div className="flex justify-between items-center px-2 w-full">
          <Link href="/">
            <a>
              <Image src="/imgs/logo.svg" width="40" height="40" className="cursor-pointer" />
            </a>
          </Link>
          {loggedIn && (
            <div className="pt-2">
              <Menu>
                {({ open }) => (
                  <>
                    <Menu.Button className="px-1 focus:outline-none ring-0">
                      <DotsHorizontal />
                    </Menu.Button>
                    <DropdownItems>
                      <Menu.Item>
                        <DropdownItem>
                          <div className="flex items-center space-x-1" onClick={() => logout()}>
                            <div className="w-1/12 mr-2">
                              <User size={20} />
                            </div>
                            <div>Logout</div>
                          </div>
                        </DropdownItem>
                      </Menu.Item>
                    </DropdownItems>
                  </>
                )}
              </Menu>
            </div>
          )}
        </div>
      </div>
      <div className="flex justify-center items-center ">
        <div>
          <Nav />
        </div>
      </div>
    </div>
  );
};
