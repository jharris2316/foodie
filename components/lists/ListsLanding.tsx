import { FieldError, FieldInput, FormActions, Input } from "components/shared/Form";
import { PlusCircle } from "heroicons-react";
import { sessionState } from "lib/auth/state";
import { myFetch } from "lib/hooks/request";
import { IList } from "lib/models/list.model";
import { useForm } from "react-hook-form";
import { useRecoilValue } from "recoil";
import { mutate, trigger } from "swr";
import { arrayMutateCreator } from "utils/swr-utils";
import { ListsPreview } from "./ListsPreview";
import classNames from "classnames/bind";
import { Add, Button } from "components/shared/Buttons";
import { Modal } from "components/shared/Modal";
import { FC, useState } from "react";
import { SavingLoader } from "components/shared/Loaders";
import { ListType } from "lib/models/list.model";
import { ListsPreviewPlaceholder } from "./ListsPreviewPlaceholder";

interface IListLanding {
  lists: IList[];
  endpoint: string;
  type: ListType;
}

export const ListsLanding: FC<IListLanding> = ({ lists, endpoint, type }) => {
  const { register, handleSubmit, errors, reset, formState } = useForm({
    mode: "onChange",
  });
  const { user } = useRecoilValue(sessionState);
  const [saving, setSaving] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);

  const onCreate = async (data) => {
    if (!data?.name) return;

    setSaving(true);

    try {
      const newList = {
        name: data.name,
        group: user?.group,
        type,
      };

      const adder = arrayMutateCreator(newList, 0);

      mutate(endpoint, adder, false);

      reset();

      await myFetch<IList>(endpoint, {
        method: "POST",
        body: JSON.stringify(newList),
        headers: {
          "Content-Type": "application/json",
        },
      });

      setModalOpen(false);

      trigger(endpoint);
    } catch (e) {
      console.log("can't add list item");
    } finally {
      setSaving(false);
    }
  };

  return (
    <>
      {lists ? <ListsPreview lists={lists || []} type={type} /> : <ListsPreviewPlaceholder />}
      <Add onClick={() => setModalOpen(true)} />
      <Modal onClose={() => setModalOpen(false)} isOpen={modalOpen}>
        <Modal.Header onClose={() => setModalOpen(false)}>
          <Modal.Title>Add {type}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={handleSubmit(onCreate)} autoComplete="off">
            <FieldInput>
              <Input
                type="text"
                name="name"
                autoComplete="off"
                autoCorrect="off"
                autoCapitalize="on"
                spellCheck="false"
                ref={register({ required: true })}
                placeholder={`Create ${type}`}
              />
            </FieldInput>
            {errors.name && <FieldError>Please enter a valid name</FieldError>}
            <FormActions>
              <Button type="submit" disabled={saving}>
                Save
              </Button>
            </FormActions>
          </form>
          {saving && <SavingLoader position="absolute" />}
        </Modal.Body>
      </Modal>
    </>
  );
};
