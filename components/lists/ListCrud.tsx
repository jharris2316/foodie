import { IList } from "lib/models/list.model";
import { mutate, trigger } from "swr";
import { endpoints, Routes } from "lib/router";
import { myFetch } from "lib/hooks/request";
import { FieldInput, Input } from "components/shared/Form";
import { Items } from "components/items/Items";
import { arrayMutateUpdater } from "utils/swr-utils";
import { PageLoader } from "components/shared/Loaders";
import { useRef, useState } from "react";
import { Card } from "components/shared/Card";
import { DocumentDuplicate, DotsHorizontal, Trash } from "heroicons-react";
import { Menu } from "@headlessui/react";
import { arrayMutateDeleter } from "utils/swr-utils";
import { useRouter } from "next/router";
import { DropdownItem, DropdownItems } from "components/shared/Dropdown";
import { ListModalSelector } from "./ListModalSelector";
import { copyList } from "lib/api";
import { ItemsPlaceholder } from "components/items/ItemsPlaceholder";

export default function ListCrud({ list, type }) {
  const router = useRouter();
  const [modalOpen, setModalOpen] = useState(false);
  const savedName = useRef(list?.name || "");
  const endpoint = `${endpoints.lists}?type=${type}`;
  const clientRoute = `/${type}s`;

  const onBlur = async (e) => {
    const name = e.target.value;

    if (!name || savedName === name) return;

    await onUpdate({ name });

    savedName.current = name;
  };

  const onUpdate = async (data) => {
    try {
      const updater = arrayMutateUpdater(list.id, { ...list, ...data });

      mutate(endpoint, updater, false);

      await myFetch<IList>(`${endpoints.lists}/${list.id}`, {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      });

      trigger(endpoint);
    } catch (e) {
      console.log(e, "cant update");
    } finally {
    }
  };

  const onDelete = async () => {
    try {
      const deleter = arrayMutateDeleter(list.id);

      mutate(endpoint, deleter, false);

      await myFetch(`${endpoints.lists}/${list.id}`, {
        method: "DELETE",
      });

      router.replace(clientRoute);
    } catch (e) {
      console.log("can't delete item");
    }
  };

  const onModalListSelect = async (selectedList) => {
    await copyList(selectedList.id, list.id);

    trigger(endpoints.listsItems(list.id));
  };

  return (
    <>
      <Card>
        <Card.InnerPageHeader backTitle={`${type}s`}>
          <Menu>
            {({ open }) => (
              <>
                <Menu.Button className="px-1 focus:outline-none ring-0 pt-2" disabled={!Boolean(list)}>
                  <DotsHorizontal />
                </Menu.Button>
                {open && !modalOpen && (
                  <DropdownItems>
                    {type === "list" && (
                      <>
                        <Menu.Item>
                          <DropdownItem>
                            <div className="flex items-center space-x-1" onClick={() => setModalOpen(true)}>
                              <div className="w-1/12 mr-2">
                                <DocumentDuplicate size={20} />
                              </div>
                              <div>Apply template</div>
                            </div>
                          </DropdownItem>
                        </Menu.Item>
                      </>
                    )}
                    <Menu.Item>
                      <DropdownItem>
                        <div
                          className="flex items-center space-x-1"
                          onClick={() => {
                            if (window.confirm("Delete the item?")) {
                              onDelete();
                            }
                          }}>
                          <div className="w-1/12 mr-2">
                            <Trash size={20} />
                          </div>
                          <div>Delete</div>
                        </div>
                      </DropdownItem>
                    </Menu.Item>
                  </DropdownItems>
                )}
              </>
            )}
          </Menu>
        </Card.InnerPageHeader>
        <Card.Body>
          <div className="relative">
            <FieldInput>
              <Input
                disabled={!Boolean(list)}
                type="text"
                name="name"
                defaultValue={list?.name}
                onBlur={onBlur}
                autoComplete="off"
                autoCorrect="off"
                autoCapitalize="on"
                spellCheck="false"
              />
            </FieldInput>

            <Items type={list?.type} updateList={onUpdate} list={list} />
          </div>
        </Card.Body>
      </Card>
      {type === "list" && (
        <ListModalSelector
          onClose={() => setModalOpen(false)}
          isOpen={modalOpen}
          onSelect={onModalListSelect}
          type="template"
        />
      )}
    </>
  );
}
