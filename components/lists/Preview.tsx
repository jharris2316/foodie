import Link from "next/link";
import { Routes } from "lib/router";
import { ViewList, Template, Cake } from "heroicons-react";
import classNames from "classnames/bind";

const routes = {
  meal: Routes.Meals,
  list: Routes.Lists,
  template: Routes.Templates,
};

const icons = {
  meal: Cake,
  list: ViewList,
  template: Template,
};

export const Preview = ({ list }) => {
  const routeBase = routes[list?.type] || Routes.Lists;
  const IconComp = icons[list?.type] || ViewList;

  if (!list?.id) return null;

  return (
    <Link href={`${routeBase}/${list.id}`}>
      <a
        className={classNames("block group relative mb-4", {
          "pointer-events-none": !Boolean(list.id),
        })}>
        {/* <div className="absolute top-1 left-1 text-gray-500">
          <IconComp size={18} />
        </div> */}
        <div className="py-4 px-6 text-center transition border border-gray-700 rounded-lg hover:border-primary hover:bg-gray-800">
          <div className="flex items-center justify-center space-x-1 text-gray-600 relative">
            <span className="text-paragraph text-sm leading-tight">{list.name}</span>
          </div>
        </div>
      </a>
    </Link>
  );
};
export const MealPreview = ({ meal }) => {
  return <Preview list={{ ...meal, type: "meal" }} />;
};
