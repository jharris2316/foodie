import { MasonryLayout } from "components/layout/Masonry";
import { InformationCircle } from "heroicons-react";
import { Preview } from "./Preview";

const noList = {
  list: {
    copy: "Lists are the main way to keep track your todos",
    link: "",
  },
  template: {
    copy: "Templates are used for reoccuring todos. eg Weekly grocery items. You can apply them to lists",
    link: "",
  },
};

export const ListsPreview = ({ lists, type }) => {
  const renderItem = (l, i) => {
    return (
      <div key={i} style={{ opacity: l.id ? 1 : 0.5 }}>
        <Preview list={l} />
      </div>
    );
  };

  if (!lists?.length) {
    return (
      <div className="flex justify-center items-center space-x-1">
        <div className="max-w-xs">
          <p className="text-sm text-center leading-tight text-gray-400">{noList?.[type]?.copy}</p>
        </div>
      </div>
    );
  }

  return <MasonryLayout items={lists} renderItem={renderItem} />;
};
