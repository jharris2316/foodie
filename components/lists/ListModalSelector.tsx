import { PageLoader } from "components/shared/Loaders";
import { IList } from "lib/models/list.model";
import { endpoints } from "lib/router";
import useSWR, { trigger } from "swr";
import { useState } from "react";
import { SavingLoader } from "components/shared/Loaders";
import classNames from "classnames/bind";
import { myFetch } from "lib/hooks/request";
import { Modal } from "components/shared/Modal";

// const useApiAdd = (endpoint, body) => {
//   const post = async(endpoint, body) => {
//     await myFetch<any>(endpoint, {
//       method: "POST",
//       body,
//       headers: {
//         "Content-Type": "application/json",
//       },
//     });
//   }

//   const response = await post(endpoint, body)
// }

export const ListModalSelector = ({ type, onClose, isOpen, onSelect: onSelectCallback }) => {
  const listEndpoint = `${endpoints.lists}/?type=${type}`;
  const { data: lists } = useSWR<IList[]>(listEndpoint);
  const [saving, setSaving] = useState(false);

  const onSelect = async (originalList) => {
    try {
      setSaving(true);

      await onSelectCallback(originalList);

      onClose();
    } catch (e) {
      // TODO handle this!!
      console.log(e);
    } finally {
      setSaving(false);
    }
  };

  const renderContent = () => {
    if (!lists) {
      return <PageLoader />;
    }
    return (
      <>
        <div className="divide-y divide-gray-700">
          {lists.map((l) => (
            <button
              disabled={saving}
              onClick={() => onSelect(l)}
              key={l.id}
              className={classNames("block w-full p-2 text-left", {
                "text-paragraph hover:bg-gray-700": !saving,
                "text-gray-500 cursor-default": saving,
              })}>
              {l.name}
            </button>
          ))}
        </div>
        {saving && <SavingLoader position="absolute" />}
      </>
    );
  };

  return (
    <Modal onClose={onClose} isOpen={isOpen}>
      <Modal.Header onClose={onClose}>
        <Modal.Title>Choose a List</Modal.Title>
      </Modal.Header>
      <Modal.Body>{renderContent()}</Modal.Body>
    </Modal>
  );
};
