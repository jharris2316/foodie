import { MasonryLayout } from "components/layout/Masonry";

export const ListsPreviewPlaceholder = ({ itemCount = 4 }) => {
  const renderItem = (l, i) => {
    return (
      <div
        className="animate-pulse mb-4"
        key={i}
        style={{
          animationDelay: `${i * 100}ms`,
        }}>
        <div className="bg-gray-600 h-10 rounded-lg"></div>
      </div>
    );
  };

  return <MasonryLayout items={Array.from({ length: itemCount })} renderItem={renderItem} />;
};
