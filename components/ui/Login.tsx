import { Button } from "components/shared/Buttons";
import { FieldError, FieldInput, FormActions, Input, Label } from "components/shared/Form";
import { sessionState } from "lib/auth/state";
import { myFetch } from "lib/hooks/request";
import { IUser } from "lib/models/user.model";
import { endpoints, Routes } from "lib/router";
import Head from "next/head";
import { useRouter } from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useSetRecoilState } from "recoil";

export const LoginForm = () => {
  const router = useRouter();
  const { register, handleSubmit, errors } = useForm();
  const [error, setError] = useState(null);
  const setSessionState = useSetRecoilState(sessionState);

  const onSubmit = async (data) => {
    setError(null);
    try {
      const user = await myFetch<IUser>(endpoints.login, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });

      setSessionState({
        user,
        resolved: true,
      });
      router.push(Routes.Home);
    } catch (error) {
      setError("Authentication failed, please try again");
    }
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <FieldInput>
        <Label>Email</Label>
        <Input type="email" name="email" ref={register({ required: true })} />
        {errors.email && <FieldError>Please enter a valid email</FieldError>}
      </FieldInput>
      <FieldInput>
        <Label>Password</Label>
        <Input type="password" name="password" ref={register({ required: true })} />
        {errors.email && <FieldError>Please enter your password</FieldError>}
      </FieldInput>
      <FormActions>
        <Button type="submit">Login</Button>
      </FormActions>
      {error && <div className="my-4 p-2 text-center bg-red-600 text-white font-bold rounded-xl">{error}</div>}
    </form>
  );
};
