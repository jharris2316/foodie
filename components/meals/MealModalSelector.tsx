import { PageLoader } from "components/shared/Loaders";
import { IList } from "lib/models/list.model";
import { endpoints } from "lib/router";
import useSWR, { trigger } from "swr";
import { useState } from "react";
import { SavingLoader } from "components/shared/Loaders";
import classNames from "classnames/bind";
import { myFetch } from "lib/hooks/request";
import { Modal } from "components/shared/Modal";
import { IMeal } from "lib/models/meal.model";

// const useApiAdd = (endpoint, body) => {
//   const post = async(endpoint, body) => {
//     await myFetch<any>(endpoint, {
//       method: "POST",
//       body,
//       headers: {
//         "Content-Type": "application/json",
//       },
//     });
//   }

//   const response = await post(endpoint, body)
// }

export const MealModalSelector = ({ onClose, isOpen, onSelect: onSelectCallback }) => {
  const endpoint = `${endpoints.meals}`;
  const { data: meals } = useSWR<IMeal[]>(endpoint);
  const [saving, setSaving] = useState(false);

  const onSelect = async (originalList) => {
    try {
      setSaving(true);
      await onSelectCallback(originalList);

      onClose();
    } catch (e) {
      // TODO handle this!!
      console.log(e);
    } finally {
      setSaving(false);
    }
  };

  const renderContent = () => {
    if (!meals) {
      return <PageLoader />;
    }
    return (
      <>
        <div className="divide-y divide-gray-700">
          {meals.map((m) => (
            <button
              onClick={() => onSelect(m)}
              key={m.id}
              className={classNames("block w-full p-2 text-left text-paragraph hover:bg-gray-700")}>
              {m.name}
            </button>
          ))}
        </div>
        {saving && <SavingLoader position="absolute" />}
      </>
    );
  };

  return (
    <Modal onClose={onClose} isOpen={isOpen}>
      <Modal.Header onClose={onClose}>
        <Modal.Title>Choose a Meal</Modal.Title>
      </Modal.Header>
      <Modal.Body>{renderContent()}</Modal.Body>
    </Modal>
  );
};
