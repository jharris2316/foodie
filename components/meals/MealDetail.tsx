import { IList } from "lib/models/list.model";
import { useRouter } from "next/router";
import { IMeal } from "lib/models/meal.model";
import { useRef, useState } from "react";
import { arrayMutateDeleter, arrayMutateUpdater } from "utils/swr-utils";
import { endpoints, Routes } from "lib/router";
import { myFetch } from "lib/hooks/request";
import useSWR, { mutate, trigger } from "swr";
import { SavingLoader } from "components/shared/Loaders";
import { Card } from "components/shared/Card";
import { DotsHorizontal, Trash } from "heroicons-react";
import { Menu } from "@headlessui/react";
import { DropdownItem, DropdownItems } from "components/shared/Dropdown";
import { FieldInput, Input, Textarea } from "components/shared/Form";
import { Items } from "components/items/Items";
import { getRefIdFromJson } from "lib/faundadb-utilities";
import { ListModalSelector } from "components/lists/ListModalSelector";
import { copyList } from "lib/api";
import { Button } from "components/shared/Buttons";
import { useToast } from "components/shared/Toast/Toast";
import { ItemsPlaceholder } from "components/items/ItemsPlaceholder";

export const MealDetail = ({ meal }) => {
  const router = useRouter();
  const [saving, setSaving] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const savedMeal = useRef(meal);
  const listId = getRefIdFromJson(meal?.listRef);
  const listEndpoint = listId ? `${endpoints.lists}/${listId}` : null;
  const { data: list } = useSWR(listEndpoint);

  const toast = useToast({ timer: 3000 });

  const onDelete = async () => {
    setSaving(true);
    try {
      const deleter = arrayMutateDeleter(meal.id);
      mutate(endpoints.meals, deleter, false);

      await myFetch(`${endpoints.meals}/${meal.id}`, {
        method: "DELETE",
      });

      router.replace(Routes.Meals);
    } catch (e) {
      //TODO handle error
      console.log("on delete error", e);
    } finally {
      setSaving(false);
    }
  };

  const onNameBlur = async (e) => {
    const name = e.target.value;
    if (!name || savedMeal?.current?.name === name) return;

    update({ name });
  };
  const onDescriptionBlur = async (e) => {
    const description = e.target.value;
    if (savedMeal?.current?.description === description) return;

    update({ description });
  };

  const update = async (newData) => {
    setSaving(true);

    try {
      const updater = arrayMutateUpdater(meal.id, { ...meal, ...newData });

      const data = {
        ...(newData || {}),
      };

      mutate(endpoints.meals, updater, false);

      const updated = await myFetch<IMeal>(`${endpoints.meals}/${meal.id}`, {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      });

      savedMeal.current = { ...updated };

      trigger(endpoints.meals);
    } catch (e) {
      console.log(e, "cant update");
    } finally {
      setSaving(false);
    }
  };

  const onModalListSelect = async (selectedList) => {
    await copyList(listId, selectedList.id);

    toast.show(`Items were copied to ${selectedList.name} list`);

    trigger(endpoints.listsItems(selectedList.id));
  };

  const onListUpdate = async (data) => {
    try {
      mutate(listEndpoint, { ...list, ...data }, false);

      await myFetch<IList>(`${endpoints.lists}/${list.id}`, {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      });
    } catch (e) {
      console.log(e, "cant update");
    } finally {
    }
  };

  return (
    <>
      <Card>
        <Card.InnerPageHeader backTitle="Meals">
          <Menu>
            {({ open }) => (
              <>
                <Menu.Button className="px-1 focus:outline-none ring-0 pt-2" disabled={!Boolean(meal)}>
                  <DotsHorizontal />
                </Menu.Button>
                {open && (
                  <DropdownItems>
                    <Menu.Item>
                      <DropdownItem>
                        <div
                          className="flex items-center space-x-1"
                          onClick={() => {
                            if (window.confirm("Delete the item?")) {
                              onDelete();
                            }
                          }}>
                          <div className="w-1/12 mr-2">
                            <Trash size={20} />
                          </div>
                          <div>Delete</div>
                        </div>
                      </DropdownItem>
                    </Menu.Item>
                  </DropdownItems>
                )}
              </>
            )}
          </Menu>
        </Card.InnerPageHeader>
        <Card.Body>
          <div className="relative">
            <FieldInput>
              <Input
                type="text"
                name="name"
                disabled={!Boolean(meal)}
                defaultValue={meal?.name}
                onBlur={onNameBlur}
                autoComplete="off"
                autoCorrect="off"
                autoCapitalize="on"
                spellCheck="false"
              />
            </FieldInput>
            <FieldInput>
              <Textarea
                onBlur={onDescriptionBlur}
                defaultValue={meal?.description}
                placeholder="Recipe"
                name="recipe"
                disabled={saving || !Boolean(meal)}
              />
            </FieldInput>
            <div className="mt-6">
              <div className="flex justify-between items-center">
                <h4 className="ml-6 mb-0">Items</h4>
                <div className="-mr-3">
                  <Button variant={"text"} onClick={() => setModalOpen(true)}>
                    add to list
                  </Button>
                </div>
              </div>
              <Items list={list} type={"meal"} updateList={onListUpdate} />
            </div>
          </div>
        </Card.Body>
      </Card>
      {saving && <SavingLoader />}
      <ListModalSelector
        onClose={() => setModalOpen(false)}
        isOpen={modalOpen}
        onSelect={onModalListSelect}
        type="list"
      />
    </>
  );
};
