import { Button } from "components/shared/Buttons";
import { FieldError, FieldInput, FormActions, Input, Textarea } from "components/shared/Form";
import { SavingLoader } from "components/shared/Loaders";
import { Modal } from "components/shared/Modal";
import { myFetch } from "lib/hooks/request";
import { IMeal } from "lib/models/meal.model";
import { endpoints } from "lib/router";
import { nanoid } from "nanoid";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { mutate, trigger } from "swr";
import { arrayMutateCreator } from "utils/swr-utils";
export const MealForm = ({ isOpen, onClose }) => {
  const { register, handleSubmit, errors, reset } = useForm();
  const [saving, setSaving] = useState(false);

  const onCreate = async (data) => {
    try {
      setSaving(true);

      const newMeal = {
        id: `client-${nanoid()}`,
        name: data.name,
        description: data.description,
      };

      const adder = arrayMutateCreator(newMeal);

      mutate(endpoints.meals, adder, false);

      await myFetch<IMeal>(endpoints.meals, {
        method: "POST",
        body: JSON.stringify(newMeal),
        headers: {
          "Content-Type": "application/json",
        },
      });

      trigger(endpoints.meals);
      onClose();
    } catch (e) {
      console.log("can't add list item");
    } finally {
      setSaving(false);
    }
  };

  return (
    <Modal onClose={onClose} isOpen={isOpen}>
      <Modal.Header onClose={onClose}>
        <Modal.Title>Add Meal</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form onSubmit={handleSubmit(onCreate)} autoComplete="off">
          <FieldInput>
            <Input
              placeholder="Meal"
              type="text"
              name="name"
              ref={register({ required: true })}
              disabled={saving}
              autoComplete="off"
              autoCorrect="off"
              autoCapitalize="on"
              spellCheck={false}
              autoFocus={true}
            />
            {errors.name && <FieldError>Please enter a valid name</FieldError>}
          </FieldInput>
          <FieldInput>
            <Textarea placeholder="Recipe" name="recipe" ref={register()} disabled={saving} />
          </FieldInput>
          <FormActions>
            <Button type="submit" disabled={saving}>
              Save
            </Button>
          </FormActions>
        </form>
        {saving && <SavingLoader position="absolute" />}
      </Modal.Body>
    </Modal>
  );
};
