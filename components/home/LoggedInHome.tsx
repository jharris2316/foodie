import { ListsPreview } from "components/lists/ListsPreview";
import { ListsPreviewPlaceholder } from "components/lists/ListsPreviewPlaceholder";
import { PageLoader } from "components/shared/Loaders";
import { PageLayout } from "components/shared/PageLayout";
import { IList } from "lib/models/list.model";
import { endpoints } from "lib/router";
import useSWR from "swr";

export const LoggedInHome = () => {
  const { data: lists } = useSWR<IList[]>(endpoints.lists);

  return <>{lists ? <ListsPreview lists={lists || []} type={"list"} /> : <ListsPreviewPlaceholder />}</>;
};
