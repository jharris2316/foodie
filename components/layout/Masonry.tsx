export const MasonryLayout = ({ items, renderItem }) => {
  if (!items?.length) return null;

  const col1Items = (items || []).filter((l, i) => !(i % 2));
  const col2Items = (items || []).filter((l, i) => i % 2);

  return (
    <div className="grid grid-cols-2 gap-4">
      <div>{(col1Items || []).map(renderItem)}</div>
      <div>
        {(col2Items || [])
          // .filter((c) => c.id)
          .map(renderItem)}
      </div>
    </div>
  );
};
