import { Checkbox } from "components/shared/Form";
import { nanoid } from "nanoid";
import classNames from "classnames/bind";
import { MenuAlt4, Tag, X } from "heroicons-react";
import { Autocomplete } from "components/shared/Search/Search";
import { Menu } from "@headlessui/react";
import { DropdownItem } from "components/shared/Dropdown";
import { categoryConfig, ItemCategory } from "lib/models/item.model";
import { motion } from "framer-motion";

export const Item = ({ onEdit, item, onDelete, onToggleComplete, onAdd, i, reorderingEnabled = false, listType }) => {
  const clientItem = item?.id.includes("client-");

  // only way submit gets triggered is if users hit enter
  const onSubmit = ({ name }) => {
    // update current item
    update({ name });

    // create an empty new item
    onAdd({ name: "", id: `client-${nanoid()}` }, i + 1);
  };

  const onBlur = (el) => {
    update({ name: el.target.value });
  };

  const onSelect = ({ name, category = "" }) => {
    update({ name, category: category });
  };

  const update = (updatedData) => {
    onEdit(item, updatedData);
  };

  const onCategorySelect = (category: ItemCategory) => {
    update({ category });
  };

  const renderCategory = () => {
    if (item.category && categoryConfig[item.category]) {
      const Icon = categoryConfig[item.category].icon;

      return <Icon size={14} />;
    }
    return <Tag size={16} />;
  };

  return (
    <div
      className={classNames("flex items-center", {
        "opacity-50": clientItem,
      })}>
      <div className="relative w-full">
        <div
          className={classNames("flex items-center space-x-2", {
            "-ml-4": listType !== "list",
          })}>
          <div className={classNames("flex items-center w-14 justify-between")}>
            {listType !== "list" && <div></div>}
            {reorderingEnabled ? (
              <div
                className={classNames("px-2 py-2 text-gray-500 -ml-2", {
                  "mr-2": listType === "list",
                  "-mr-1": listType !== "list",
                })}>
                <MenuAlt4 size={16} />
              </div>
            ) : (
              <div></div>
            )}

            {listType === "list" ? (
              <Checkbox
                name="complete"
                checked={item.completed}
                onChange={(e) => onToggleComplete(item, e.target.checked)}
                disabled={clientItem}
              />
            ) : null}
          </div>
          <div className="flex flex-1 items-center">
            <Autocomplete
              onSubmit={onSubmit}
              onSelect={onSelect}
              onBlur={onBlur}
              index="Item"
              type="text"
              placeholder="Edit item"
              resetOnSelect={false}
              autoFocus={item?.focusInput}
              defaultValue={item.name}
            />
          </div>
        </div>
        <div className="absolute top-2 right-0 flex justify-items-center">
          {!item.completed && (
            <Menu>
              {({ open }) => (
                <>
                  <Menu.Button className="text-gray-500 hover:text-gray-400 p-1 mr-2">{renderCategory()}</Menu.Button>
                  {open && (
                    <Menu.Items
                      static
                      className="absolute bg-gray-800 border border-gray-700 divide-gray-700 divide-opacity-50 divide-y rounded-md shadow-lg w-40 focus:outline-none right-1 -bottom-24 z-40">
                      {Object.keys(categoryConfig)
                        .map((key) => ({ key: key, ...categoryConfig[key] }))
                        .map((c) => (
                          <Menu.Item
                            key={c.key}
                            as={motion.div}
                            animate={{ opacity: 1 }}
                            initial={{ opacity: 0 }}
                            transition={{ duration: 0.2 }}>
                            <button type="button" onClick={() => onCategorySelect(c.key)} className="block w-full">
                              <DropdownItem condensed={true}>
                                <div className="flex items-center space-x-2">
                                  <div className="w-1/12 mr-1">
                                    <c.icon size={12} />
                                  </div>
                                  <div>{c.name}</div>
                                </div>
                              </DropdownItem>
                            </button>
                          </Menu.Item>
                        ))}
                    </Menu.Items>
                  )}
                </>
              )}
            </Menu>
          )}
          <button
            type="button"
            onClick={() => onDelete(item?.id)}
            disabled={clientItem}
            className="text-gray-600 hover:text-gray-500 p-1">
            <X size={20} />
          </button>
        </div>
      </div>
    </div>
  );
};
