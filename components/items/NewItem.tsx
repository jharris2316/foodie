import { memo } from "react";
import { nanoid } from "nanoid";
import { Plus } from "heroicons-react";
import { Autocomplete } from "components/shared/Search/Search";

export const NewItem = memo<any>(({ onAdd }) => {
  const onSubmit = ({ name, category = "" }) => {
    onAdd({
      name,
      category,
      id: `client-${nanoid()}`,
    });
  };

  return (
    <div className="flex justify-end">
      <div className="w-14 mr-2 text-gray-500 flex justify-end items-center">
        <Plus size={22} className="-mt-2" />
      </div>

      <div className="flex-1 flex">
        <Autocomplete onSubmit={onSubmit} onSelect={onSubmit} index="Item" type="text" placeholder="Add new item" />
      </div>
    </div>
  );
});
