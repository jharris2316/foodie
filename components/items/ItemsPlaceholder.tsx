export const ItemsPlaceholder = ({ itemCount = 5 }) => {
  return (
    <div className="mx-2 my-4 ">
      <div className="space-y-4">
        {Array.from({ length: itemCount }).map((u, i) => (
          <div
            className="animate-pulse flex justify-items-between space-x-3 items-center"
            key={i}
            style={{
              animationDelay: `${i * 100}ms`,
            }}>
            <div className="bg-gray-600 h-4 w-6"></div>
            <div className="bg-gray-600 h-8 w-8 rounded-md"></div>
            <div className="bg-gray-600 h-8 flex-1 rounded"></div>
            <div className="bg-gray-600 h-6 w-6 rounded-full"></div>
            <div className="bg-gray-600 h-6 w-6 rounded-full"></div>
          </div>
        ))}
      </div>
    </div>
  );
};
