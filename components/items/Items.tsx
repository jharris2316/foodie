import { sessionState } from "lib/auth/state";
import { myFetch } from "lib/hooks/request";
import { categoryConfig, IItem } from "lib/models/item.model";
import { endpoints } from "lib/router";
import { useEffect, useState } from "react";
import { useRecoilValue } from "recoil";
import useSWR, { mutate, trigger } from "swr";
import { arrayMutateCreator, arrayMutateDeleter, arrayMutateUpdater } from "utils/swr-utils";
import { Item } from "./Item";
import { NewItem } from "./NewItem";
import { Droppable, Draggable, DragDropContext } from "react-beautiful-dnd";
import { PageLoader } from "components/shared/Loaders";
import classNames from "classnames/bind";
import { Button } from "components/shared/Buttons";
import { SwitchVertical } from "heroicons-react";
import { ItemsPlaceholder } from "./ItemsPlaceholder";

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex): any => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const sortByOrder = (a, b) => a.order - b.order;
const sortByCategory = (a, b) => {
  const orderA = categoryConfig?.[a.category]?.order ?? 100;
  const orderB = categoryConfig?.[b.category]?.order ?? 100;

  return orderA - orderB;
};

export const Items = ({ type, updateList = (vals) => null, list }) => {
  const completedEndpoint = list?.id ? `${endpoints.listsItems(list?.id)}?completed=1&limit=20` : null;
  const itemsEndpoint = list?.id ? endpoints.listsItems(list?.id) : null;
  const { data: items, mutate: itemsMutate } = useSWR<IItem[]>(itemsEndpoint);
  const { data: archivedItems } = useSWR<IItem[]>(completedEndpoint);
  const { user } = useRecoilValue(sessionState);
  const [expanded, setExpanded] = useState(false);
  const [applyCategorySort, setApplyCategorySort] = useState(list?.sortPreference === "category");

  useEffect(() => {
    return () => {
      trigger(itemsEndpoint);
    };
  }, []);

  const update = async (item, data: Partial<IItem>) => {
    // setSaving(true);
    try {
      const updater = arrayMutateUpdater(item.id, { ...item, ...data });
      await mutate(itemsEndpoint, updater, false);

      await myFetch(`${endpoints.items}/${item.id}`, {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      });

      // triggers.forEach((t) => trigger(t));
    } catch (e) {
      //TODO handle error
      console.log("on update error", e);
    } finally {
      // setSaving(false);
    }
  };

  const onAdd = async (data: Partial<IItem>, index?: number) => {
    const requiresReorder = index !== undefined ? true : false;
    const addIndex = requiresReorder ? index : items.length;
    // setSaving(true);

    try {
      const newItem = {
        ...data,
        group: user?.group,
        order: items?.length || 0,
        listId: list.id,
        optimistic: true,
        focusInput: requiresReorder,
      };

      const updatedItems = await mutate(
        itemsEndpoint,
        (items) => {
          if (!requiresReorder || !items?.length) return [...(items || []), newItem];

          items.splice(addIndex, 0, newItem);

          return items.map((item, i) => ({ ...item, order: i }));
        },
        false,
      );

      const createdItem = await myFetch<IItem>(endpoints.items, {
        method: "POST",
        body: JSON.stringify(newItem),
        headers: {
          "Content-Type": "application/json",
        },
      });

      const newIds = updatedItems.map((item, i) => {
        if (i === addIndex) return { ...item, id: createdItem.id, focusInput: requiresReorder };
        return item;
      });

      itemsMutate(newIds, false);

      if (requiresReorder) {
        await saveReorder(newIds);
      }
    } catch (e) {
      //TODO handle error
      console.log("on add error", e);
    } finally {
      // setSaving(false);
    }
  };

  const onEdit = async (item, data: Partial<IItem>) => {
    const updateItem = {
      ...data,
    };

    await update(item, updateItem);
  };

  const onDelete = async (id, endpoint) => {
    // setSaving(true);
    try {
      const deleter = arrayMutateDeleter(id);
      mutate(endpoint, deleter, false);

      await myFetch(`${endpoints.items}/${id}`, {
        method: "DELETE",
      });
    } catch (e) {
      //TODO handle error
      console.log("on delete error", e);
    } finally {
      // setSaving(false);
    }
  };

  const onToggleComplete = async (item, completed) => {
    const updateItem: Partial<IItem> = {
      completed,
    };

    if (!completed) {
      updateItem.order = items?.length;
    }

    const addFunction = arrayMutateCreator({ ...item, completed });

    const removeFunction = arrayMutateDeleter(item.id);

    mutate(itemsEndpoint, completed ? removeFunction : addFunction, false);

    mutate(completedEndpoint, completed ? addFunction : removeFunction, false);

    await update(item, updateItem);
  };

  const onDragEnd = async (result) => {
    const { destination, source, draggableId } = result;
    // dropped outside the list
    if (!destination) {
      return;
    }

    if (destination.droppableId === source.droppableId && destination.index === source.index) {
      return;
    }

    setApplyCategorySort(false);
    updateList({ sortPreference: null });

    const reorderedItems: IItem[] = reorder(items, result.source.index, result.destination.index).map((item, i) => ({
      ...item,
      order: i,
    }));

    itemsMutate(reorderedItems, false);

    await saveReorder(reorderedItems);
  };

  const saveReorder = async (items: IItem[]) => {
    const reorderedIds = items.map((i) => i.id);
    try {
      await myFetch(endpoints.itemsReorder, {
        method: "PUT",
        body: JSON.stringify({
          reorderedIds,
        }),
        headers: {
          "Content-Type": "application/json",
        },
      });

      // trigger(endpoints.items);
    } catch (e) {
      // TODO handler error
      console.log(e);
    } finally {
      // setSaving(false);
    }
  };

  const deleteAll = async (completed = true) => {
    // setSaving(true);
    try {
      const refreshEndpoint = completed ? completedEndpoint : itemsEndpoint;
      mutate(refreshEndpoint, [], false);

      await myFetch(itemsEndpoint, {
        method: "DELETE",
        body: JSON.stringify({
          completed: true,
        }),
        headers: {
          "Content-Type": "application/json",
        },
      });
    } catch (e) {
      //TODO handle error
      console.log("on delete error", e);
    } finally {
      // setSaving(false);
    }
  };

  const toggleOrderByCategory = async () => {
    const sortPreference = !applyCategorySort ? "category" : null;

    updateList({ sortPreference });
    setApplyCategorySort((c) => !c);
  };

  const sortFunction = applyCategorySort ? sortByCategory : sortByOrder;

  return (
    <div className="mx-2 my-4">
      <div className="flex justify-end">
        <button
          disabled={Boolean(items?.length < 1) || !Boolean(list)}
          onClick={toggleOrderByCategory}
          className={classNames("p-1 pr-1.5 mr-7 opacity-90 hover:opacity-100", {
            "text-secondary cursor-pointer": applyCategorySort,
            "text-gray-500 cursor-pointer": !applyCategorySort && Boolean(items?.length > 0),
            "text-gray-600 cursor-default": Boolean(items?.length < 1),
          })}>
          <SwitchVertical />
        </button>
      </div>
      {items ? (
        <DragDropContext onDragEnd={onDragEnd}>
          <Droppable droppableId="itemsDroppable">
            {(provided, snapshot) => (
              <div {...provided.droppableProps} ref={provided.innerRef}>
                {(items || []).sort(sortFunction).map((item, i) => (
                  <Draggable key={item.id} draggableId={item.id} index={i}>
                    {(provided, snapshot) => (
                      <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                        <Item
                          listType={type}
                          onEdit={(item, data) => onEdit(item, data)}
                          item={item}
                          onDelete={(id) => onDelete(id, itemsEndpoint)}
                          onToggleComplete={onToggleComplete}
                          onAdd={onAdd}
                          reorderingEnabled={true}
                          i={i}
                        />
                      </div>
                    )}
                  </Draggable>
                ))}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </DragDropContext>
      ) : (
        <ItemsPlaceholder />
      )}
      <NewItem onAdd={onAdd} />

      {type === "list" && Boolean(list) && (
        <div className="mt-6">
          <div className="flex justify-between items-center">
            <h4 className="ml-6 mb-2">Completed</h4>
            {archivedItems?.length > 0 && (
              <div className="-mr-4">
                <Button
                  variant={"text"}
                  onClick={() => {
                    if (window.confirm("Are you sure you want to delete all the completed items?")) {
                      deleteAll();
                    }
                  }}>
                  delete
                </Button>
              </div>
            )}
          </div>
          <div
            className={classNames("overflow-hidden relative", {
              "max-h-28": !expanded,
              "": expanded,
            })}>
            {!expanded && archivedItems?.length > 2 && (
              <div
                className="inset-0 bg-gradient-to-t from-gray-800 absolute z-10 flex justify-center items-end cursor-pointer"
                onClick={() => setExpanded(true)}>
                <div className="mb-2">
                  <Button variant="text">view more</Button>
                </div>
              </div>
            )}
            {archivedItems &&
              (archivedItems || []).map((item, i) => (
                <Item
                  listType={type}
                  key={`${item?.id || ""}-${i}`}
                  onEdit={(item, data) => onEdit(item, data)}
                  item={item}
                  onDelete={(id) => onDelete(id, completedEndpoint)}
                  onToggleComplete={onToggleComplete}
                  onAdd={onAdd}
                  i={i}
                />
              ))}
          </div>
        </div>
      )}
    </div>
  );
};
