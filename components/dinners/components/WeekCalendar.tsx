import { useEffect, useState } from "react";
import { add, format } from "date-fns";
import { Day } from "./Day";
import { MealModalSelector } from "components/meals/MealModalSelector";
import id from "date-fns/esm/locale/id/index.js";
import { IMeal } from "lib/models/meal.model";
import { IDinner } from "lib/models/dinner.model";

const DAYS = 7;

// // schedule
// id;
// dinnerAt: Date;
// mealRef: Ref;

export interface IDay {
  date: Date;
  dinner: IDinner;
}

export const WeekCalendar = ({ week, currentDate, dinners, onAdd: onAddProp, onDelete }) => {
  const [days, setDays] = useState<IDay[]>([]);
  const [addDate, setAddDate] = useState(null);

  useEffect(() => {
    const days: IDay[] = [...new Array(DAYS)].map((m, i) => {
      const day = add(week, { days: i });
      const dayKey = format(day, "yyyy-MM-dd");
      const dinner = (dinners || []).find((d) => d.key === dayKey);

      return {
        date: day,
        dinner: dinner || null,
      };
    });

    setDays(days);
  }, [week, dinners]);

  const onAdd = async ({ day, isTakeout = false }) => {
    if (isTakeout) {
      onAddProp({ day, isTakeout });
    } else {
      setAddDate({ day, isTakeout });
    }
  };

  return (
    <>
      <div className="space-y-2 py-3">
        {days.map((d) => (
          <Day key={d.date.toString()} day={d} onAdd={onAdd} onDelete={onDelete} currentDate={currentDate} />
        ))}
      </div>
      <MealModalSelector
        onClose={() => setAddDate(null)}
        isOpen={Boolean(addDate)}
        onSelect={(meal) => onAddProp({ ...addDate, meal })}
      />
    </>
  );
};
