import { getWeek, differenceInWeeks } from "date-fns";
import { ArrowLeft, ArrowRight } from "heroicons-react";
import classNames from "classnames/bind";

export const WeekSelector = ({ onWeekChange, activeDate, dateRange, currentDate, week }) => {
  const getWeekLabel = (active) => {
    const activeWeek = getWeek(active);
    const currentWeek = getWeek(currentDate);
    const absoluteWeeks = Math.abs(activeWeek - currentWeek);

    // current week
    if (activeWeek === currentWeek) {
      return "This Week";
    }

    if (activeWeek > currentWeek) {
      const label = absoluteWeeks > 1 ? `In ${absoluteWeeks} Weeks` : "Next Week";
      return label;
    }

    if (activeWeek < currentWeek) {
      const suffix = absoluteWeeks > 1 ? "Weeks" : "Week";
      return `${absoluteWeeks} ${suffix} Ago`;
    }

    return "";
  };
  return (
    <div className="flex justify-center space-x-2 items-center">
      <button
        onClick={() => onWeekChange(-1)}
        className={classNames("py-1", {
          "text-gray-600": week < 1,
        })}
        disabled={week < 1}>
        <ArrowLeft size={20} />
      </button>
      <span className="text-xs w-24 text-center">{getWeekLabel(activeDate)}</span>
      <button
        onClick={() => onWeekChange(1)}
        className={classNames("py-1", {
          "text-gray-600": week === dateRange.length - 1,
        })}
        disabled={week === dateRange.length - 1}>
        <ArrowRight size={20} />
      </button>
    </div>
  );
};
