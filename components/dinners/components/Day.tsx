import { format, isSameDay } from "date-fns";
import classNames from "classnames/bind";
import { Plus, X } from "heroicons-react";
import { Chinese } from "components/shared/CustomIcons";
import Link from "next/link";
import { Routes } from "lib/router";
import { getRefIdFromJson } from "lib/faundadb-utilities";

export const Day = ({ day, currentDate, onAdd, onDelete }) => {
  const currentDay = isSameDay(day.date, currentDate);
  const mealId = day?.dinner?.mealRef ? getRefIdFromJson(day?.dinner?.mealRef) : null;

  return (
    <div
      className={classNames("flex py-2 px-3 space-x-2  rounded-xl relative", {
        "bg-opacity-100 bg-gray-700": currentDay,
        "bg-gray-800 bg-opacity-70": !currentDay,
      })}>
      <div className={classNames("flex justify-center items-center flex-col absolute")}>
        <div className="uppercase text-2xl font-bold leading-none text-gray-300">{format(day.date, "dd")}</div>
        <div className="uppercase text-sm font-bold leading-none text-secondary-light text-opacity-75">
          {format(day.date, "E")}
        </div>
      </div>

      <div className="flex flex-1 h-16 relative justify-center items-center">
        {day.dinner ? (
          <>
            <div className="flex px-14 w-full justify-center">
              {day.dinner.isTakeout && (
                <span className="pr-2">
                  <Chinese size={20} />
                </span>
              )}
              {!day.dinner.isTakeout && mealId ? (
                <Link href={`${Routes.Meals}/${mealId}`}>
                  <a>
                    <span className="text-center leading-tight text-lg font-bold inline-block">{day.dinner.name}</span>
                  </a>
                </Link>
              ) : (
                <span className="text-center leading-tight text-lg font-bold inline-block">{day.dinner.name}</span>
              )}
            </div>
            <div className="absolute -right-2 -top-1">
              <button
                type="button"
                onClick={() => {
                  if (window.confirm("Delete this dinner?")) {
                    onDelete(day.dinner);
                  }
                }}
                className="text-gray-600 hover:text-gray-500 p-1">
                <X size={20} />
              </button>
            </div>
          </>
        ) : (
          <div className="flex justify-center items-center flex-1">
            <button onClick={() => onAdd({ day: day.date })} className="text-primary hover:text-primary-light p-1">
              <Plus size={36} />
            </button>

            <div className="absolute -bottom-1 -right-2">
              <button
                className="text-gray-500 hover:text-gray-400 p-1 flex items-center"
                onClick={() => onAdd({ day: day.date, isTakeout: true })}>
                <span className="text-xs pr-1">Lazy?</span>
                <Chinese size={16} />
              </button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

// Paginate(Match(Index("lists_search_by_type_and_groupRef_sort_by_createdAt_desc"), [type, groupRef])),
// Map(
//   Paginate(
//     Range(
//       Match(
//         Index("dinners_search_by_groupRef_sort_by_dinnerAt"), Ref(Collection("groups"), "290305145515803145")
//       ),
//       Date("2021-02-01"),
//       Date("2021-03-07")
//     )
//   ),
//   Lambda(["dinnerAt", "ref"], Get(Var("ref"))),
// )

// Map(
//   Paginate(
//     Match(
//       Index("meals_search_by_groupRef_sort_by_name"),
//       Ref(Collection("groups"), "290305145515803145")
//     )
//   ),
//   Lambda(["name", "ref"], Get(Var("ref")))
// )
