export const WeekPlaceholder = ({ itemCount = 7 }) => {
  return (
    <div className="mx-2 my-4 ">
      <div className="space-y-4">
        {Array.from({ length: itemCount }).map((u, i) => (
          <div
            className="animate-pulse space-x-2"
            key={i}
            style={{
              animationDelay: `${i * 100}ms`,
            }}>
            <div className="bg-gray-600 h-16 flex-1 rounded-xl"></div>
          </div>
        ))}
      </div>
    </div>
  );
};
