/**
 * Careful renaming routes, we wrap the app with css classes based on theses
 * currently for certain style changes to the header mostly, so changing them
 * might break some styling
 */
export enum Routes {
  // guest routes
  Login = "/login",

  // authed routes
  Home = "/",
  Lists = "/lists",
  Templates = "/templates",
  Meals = "/meals",
  Dinners = "/dinners",
}

const base = "/api";

export const endpoints = {
  login: `${base}/login`,
  logout: `${base}/logout`,
  user: `${base}/user`,
  lists: `${base}/lists`,
  listsItems: (listId) => `${base}/lists/${listId}/items`,
  listsItemsCopy: (listId) => `${base}/lists/${listId}/copy-items`,
  items: `${base}/items`,
  itemsReorder: `${base}/items/reorder`,
  meals: `${base}/meals`,
  dinners: `${base}/dinners`,
  searchIndex: (index) => `${base}/search/${index}`,
};
