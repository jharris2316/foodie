import algoliasearch from "algoliasearch";

const adminClient = algoliasearch(process.env.ALGOLIA_APPLICATION_ID, process.env.ALGOLIA_ADMIN_KEY);

export const addRecord = async (index, data, user) => {
  const clientIndex = adminClient.initIndex(`prod_${index}`);
  try {
    await clientIndex.saveObject({
      objectID: data?.id,
      ...data,
      user: {
        id: user?.id || "",
        name: user?.name || "",
      },
    });
  } catch (e) {
    console.log("unable to add record to algolia");
    console.error(e);
  }
};

export const updateRecord = async (index, id, data) => {
  const clientIndex = adminClient.initIndex(`prod_${index}`);
  try {
    await clientIndex.partialUpdateObject({
      objectID: id,
      ...(data || {}),
    });
  } catch (e) {
    console.error(e);
  }
};

export const deleteRecord = async (index, id) => {
  const clientIndex = adminClient.initIndex(`prod_${index}`);
  try {
    await clientIndex.deleteObject(id);
  } catch (e) {
    console.error(e);
  }
};
