import { documentsTransform } from "lib/faunadb";
import { getRef } from "lib/faundadb-utilities";
import { BaseModel } from "./base.model";
import { IUserRef } from "./base.model";
import { IGroupRef } from "./group.model";
import faunadb from "faunadb";

const { Map, Paginate, Lambda, Get, Match, Index, Var } = faunadb.query;

export type ListType = "list" | "template" | "meal";
export interface IList extends IUserRef, IGroupRef {
  id?: string;
  name: string;
  createdAt: any;
  updatedAt: any;
  type: ListType;
  sortPreference?: "category";
}

const COLLECTION_NAME = "lists";

export class ListModel extends BaseModel {
  constructor(token) {
    super(token, COLLECTION_NAME);
  }

  public async getRef(id) {
    return await getRef(COLLECTION_NAME, id);
  }

  public async getLists(type: ListType = "list", groupRef): Promise<IList[]> {
    const { data } = await this.client.query(
      Map(
        Paginate(Match(Index("lists_search_by_type_and_groupRef_sort_by_createdAt_desc"), [type, groupRef])),
        Lambda(["createdAt", "ref"], Get(Var("ref"))),
      ),
    );

    return documentsTransform(data);
  }
}
