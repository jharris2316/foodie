import { format } from "date-fns";
import { documentsTransform, q, transform } from "lib/faunadb";
import { BaseModel } from "./base.model";
import { IUserRef } from "./base.model";
import { IGroupRef } from "./group.model";

const { Map, Paginate, Lambda, Collection, Get, Ref, Match, Index, Var, Range, Date } = q;

export interface IDinner extends IUserRef, IGroupRef {
  id?: string;
  // fauna ref
  ref?: any;
  // date meal is set for
  dinnerAt: any;
  mealRef?: any;
  name: string;
  isTakeout?: boolean;
  createdAt: any;
  updatedAt: any;
}

const COLLECTION_NAME = "dinners";

export class DinnerModel extends BaseModel {
  constructor(token) {
    super(token, COLLECTION_NAME);
  }

  public async get(groupRef): Promise<IDinner[]> {
    const { data } = await this.client.query(
      Map(
        Paginate(Match(Index("meals_search_by_groupRef_sort_by_name"), groupRef)),
        Lambda(["name", "ref"], Get(Var("ref"))),
      ),
    );

    return documentsTransform(data);
  }

  public async getDinnersByRange(groupRef, startAt: string, endAt: string): Promise<IDinner[]> {
    const { data } = await this.client.query(
      Map(
        Paginate(
          Range(Match(Index("dinners_search_by_groupRef_sort_by_dinnerAt"), groupRef), Date(startAt), Date(endAt)),
        ),
        Lambda(["dinnerAt", "ref"], Get(Var("ref"))),
      ),
    );

    return data
      .filter((d) => d?.ref?.id)
      .map((d) => {
        return { ...transform(d), key: d.data.dinnerAt?.value || null };
      });
  }
}
