import { getClient, transform } from "lib/faunadb";
import { getById, getDocuments, create, update, remove } from "lib/faundadb-utilities";

export interface IUserRef {
  userRef: any;
}

export class BaseModel {
  public collection = null;
  public client = null;

  constructor(token, collection) {
    this.collection = collection;
    this.client = getClient(token);
  }

  // // TODO this needs to handle pagiantion
  // public async get<T>(): Promise<T[]> {
  //   const { data } = await getDocuments(this.client, this.collection);
  //   return data.map(transform);
  // }

  public async getById<T>(id): Promise<T> {
    const result = await getById(this.client, this.collection, id);
    return transform(result);
  }

  public async create<T>(data: Partial<T>) {
    const res = await create(this.client, this.collection, data);
    return transform(res);
  }

  public async update<T>(id, data: Partial<T>) {
    const updated = await update(this.client, this.collection, id, data);
    return transform(updated);
  }

  public async delete(id) {
    await remove(this.client, this.collection, id);
  }
}
