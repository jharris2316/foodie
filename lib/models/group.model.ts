import { getRef } from "lib/faundadb-utilities";
import { q } from "../faunadb";

export interface IGroupRef {
  groupRef: any;
}

export interface IUser {
  id?: string;
  name: string;
  email: string;
  group: any;
}

const COLLECTION_NAME = "groups";

export class GroupModel {
  // async createUser(email) {
  //   return serverClient.query(
  //     q.Create(q.Collection(COLLECTION_NAME), {
  //       data: { email },
  //     }),
  //   );
  // }

  public async getRef(groupId) {
    return await getRef(COLLECTION_NAME, groupId);
  }
}
