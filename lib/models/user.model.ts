import { q, serverClient, transform } from "../faunadb";
import { IGroupRef } from "./group.model";

const COLLECTION_NAME = "users";

export interface IUser {
  id?: string;
  name: string;
  email: string;
  group: string;
}

export class UserModel {
  // async createUser(email) {
  //   return serverClient.query(
  //     q.Create(q.Collection(COLLECTION_NAME), {
  //       data: { email },
  //     }),
  //   );
  // }

  async getUserByEmail(email): Promise<IUser> {
    const user = await serverClient.query(q.Get(q.Match(q.Index("users_by_email"), email)));
    // console.log("get user", user);
    return transform(user);
  }

  async getUserRef(userId) {
    return await q.Ref(q.Collection(COLLECTION_NAME), userId);
  }
}
