import { Bakery, CannedGoods, Dairy, Frozen, Grains, Health, Meat, Produce } from "components/shared/CustomIcons";
import { q, documentsTransform } from "lib/faunadb";
import { getDocumentsByFilter, getRef } from "lib/faundadb-utilities";
import { BaseModel } from "./base.model";
import { IUserRef } from "./base.model";
import { IGroupRef } from "./group.model";
import { QuestionMarkCircleOutline } from "heroicons-react";

export interface IItem extends IUserRef, IGroupRef {
  id?: string;
  name: string;
  order: number;
  listRef: any;
  createdAt: any;
  updatedAt: any;
  completed: boolean;
  category?: ItemCategory;
}

export enum ItemCategory {
  Dairy = "Dairy",
  Meat = "Meat",
  Produce = "Produce",
  CannedGoods = "CannedGoods",
  /**
   * Grains, pasta, sides
   */
  GrainsPasta = "GrainsPasta",
  Bakery = "Bakery",
  Frozen = "Frozen",
  Health = "Health",
  Other = "Other",
}

export const categoryConfig = {
  [ItemCategory.Produce]: {
    order: 0,
    icon: Produce,
    name: "Produce",
  },
  [ItemCategory.Meat]: {
    order: 1,
    icon: Meat,
    name: "Meat",
  },
  [ItemCategory.Dairy]: {
    order: 2,
    icon: Dairy,
    name: "Dairy",
  },
  [ItemCategory.GrainsPasta]: {
    order: 3,
    icon: Grains,
    name: "Grains/Pasta",
  },
  [ItemCategory.CannedGoods]: {
    order: 4,
    icon: CannedGoods,
    name: "Canned Goods",
  },
  [ItemCategory.Health]: {
    order: 5,
    icon: Health,
    name: "Health",
  },
  [ItemCategory.Frozen]: {
    order: 6,
    icon: Frozen,
    name: "Frozen",
  },
  [ItemCategory.Bakery]: {
    order: 7,
    icon: Bakery,
    name: "Bakery",
  },
  [ItemCategory.Other]: {
    order: 8,
    icon: QuestionMarkCircleOutline,
    name: "Other",
  },
};

const COLLECTION_NAME = "items";

const { Map, Paginate, Documents, Lambda, Collection, Get, Ref, Create, Update, Delete, Match, Index, Var, Now } = q;

export class ItemModel extends BaseModel {
  constructor(token) {
    super(token, COLLECTION_NAME);
  }

  public getItemsByListId = async <T>(listId, completed = false, options = {}): Promise<T[]> => {
    const listRef = await getRef("lists", listId);

    if (!listRef) {
      throw new Error("Invalid list id provided");
    }

    const { data } = await getDocumentsByFilter(
      this.client,
      "items_search_by_listRef_and_completed",
      [listRef, completed],
      options,
    );
    return documentsTransform(data);
  };

  public deleteItemsByListId = async <T>(listId, completed = true): Promise<T[]> => {
    const listRef = await getRef("lists", listId);

    if (!listRef) {
      throw new Error("Invalid list id provided");
    }

    return this.client.query(
      Map(
        Paginate(Match(Index("items_search_by_listRef_and_completed"), [listRef, completed]), { size: 500 }),
        Lambda("ref", Delete(Var("ref"))),
      ),
    );
  };
}
