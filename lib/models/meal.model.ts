import { documentsTransform, q } from "lib/faunadb";
import { BaseModel } from "./base.model";
import { IUserRef } from "./base.model";
import { IGroupRef } from "./group.model";
import { getById } from "lib/faundadb-utilities";

const { Map, Paginate, Lambda, Collection, Get, Ref, Match, Index, Var } = q;

export interface IMeal extends IUserRef, IGroupRef {
  id?: string;
  // fauna ref
  ref?: any;
  name: string;
  description: string;
  nutrition?: {
    carbs: number;
    protein: number;
    fat: number;
  };
  listRef: any;
  createdAt: any;
  updatedAt: any;
}

const COLLECTION_NAME = "meals";

export class MealModel extends BaseModel {
  constructor(token) {
    super(token, COLLECTION_NAME);
  }

  public async get(groupRef): Promise<IMeal[]> {
    const { data } = await this.client.query(
      Map(
        Paginate(Match(Index("meals_search_by_groupRef_sort_by_name"), groupRef)),
        Lambda(["name", "ref"], Get(Var("ref"))),
      ),
    );

    return documentsTransform(data);
  }
  public async getByIdWithList(id): Promise<IMeal> {
    const result = await getById(this.client, COLLECTION_NAME, id);
    const meal = documentsTransform(result);
    return meal;
  }
}
