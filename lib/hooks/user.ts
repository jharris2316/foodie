import { isLoggedInSelector } from "lib/auth/state";
import { endpoints } from "lib/router";
import { useEffect, useState } from "react";
import { useRecoilValue } from "recoil";
import useSWR from "swr";

export function useUser() {
  const { data, isValidating } = useSWR(endpoints.user);
  const user = data?.user ?? null;
  return { user, loading: isValidating };
}

export const useIsLoggedIn = (): boolean => {
  const isLoggedIn = useRecoilValue(isLoggedInSelector);

  return isLoggedIn;
};
