import { useEffect, useRef, useState } from "react";

// same as fetch but just returns data and throws error if response not ok
export const myFetch = async <T extends object>(url: string, init?: RequestInit): Promise<T> => {
  try {
    const response = await fetch(url, init);
    if (!response.ok) {
      const error = new Error("An error occurred while fetching the data.");
      (error as any).status = response.status;
      throw error;
    }

    const data = await response.json();
    return data;
  } catch (e) {
    const error = new Error(e.message);
    (error as any).status = e.status;
    throw error;
  }
};

// type status = "idle" | "fetching" | "fetched";
// export const useFetch = (url: string, init?: RequestInit) => {
//   const cache = useRef({});
//   const [status, setStatus] = useState<status>("idle");
//   const [data, setData] = useState([]);
//   const options = init || {};

//   useEffect(() => {
//     if (!url) return;
//     const fetchData = async () => {
//       setStatus("fetching");
//       if (cache.current[url]) {
//         const data = cache.current[url];
//         setData(data);
//         setStatus("fetched");
//         return;
//       }

//       try {
//         const response = await fetch(url, options);
//         if (!response.ok) {
//           const error = new Error("An error occurred while fetching the data.");
//           (error as any).status = response.status;
//           throw error;
//         }

//         const data = await response.json();
//         cache.current[url] = data;
//         setData(data);
//         setStatus("fetched");
//       } catch (e) {
//         const error = new Error(e.message);
//         (error as any).status = e.status;
//         throw error;
//       }
//     };

//     fetchData();
//   }, [url, options]);

//   return { status, data };
// };
