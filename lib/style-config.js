// export const colors = {
//   primary
// }

const colors = {
  primary: {
    light: "#A78BFA",
    DEFAULT: "#7E3DE2",
    dark: "#5B21B6",
  },
  secondary: {
    light: "#FBD962",
    DEFAULT: "#F4CD41",
    dark: "#EAC339",
  },
  paragraph: {
    light: "#F2F2F2",
    DEFAULT: "#DAD8D8",
    dark: "#A0A0A0",
  },
  headers: {
    light: "#F2F2F2",
    DEFAULT: "#DAD8D8",
    dark: "#A0A0A0",
  },
};

module.exports = {
  colors,
};
