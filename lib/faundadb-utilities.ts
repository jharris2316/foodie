import { q } from "./faunadb";

const { Map, Paginate, Documents, Lambda, Collection, Get, Ref, Create, Update, Delete, Match, Index, Var, Now } = q;

export const getDocuments = async (client, collection) => {
  return client.query(
    Map(
      Paginate(Documents(Collection(collection))),
      Lambda((x) => Get(x)),
    ),
  );
};

export const getDocumentsByFilter = async (client, index, terms, options) => {
  return client.query(
    Map(Paginate(Match(Index(index), terms), { size: options?.limit || 100 }), Lambda("ref", Get(Var("ref")))),
  );
};

export const getById = async (client, collection, id) => {
  return client.query(Get(Ref(Collection(collection), id)));
};

export const create = async (client, collection, data, withTimestamps = true) => {
  return client.query(
    Create(Collection(collection), {
      data: {
        ...data,
        ...(withTimestamps
          ? {
              createdAt: Now(),
              updatedAt: Now(),
            }
          : {}),
      },
    }),
  );
};

export const update = async (client, collection, id, data, withTimestamps = true) => {
  return client.query(
    Update(Ref(Collection(collection), id), {
      data: {
        ...data,
        ...(withTimestamps
          ? {
              updatedAt: Now(),
            }
          : {}),
      },
    }),
  );
};

export const remove = async (client, collection, id) => {
  return client.query(Delete(Ref(Collection(collection), id)));
};

export const getRef = async (collection, id) => {
  return Ref(Collection(collection), id);
};

export const getRefIdFromJson = (ref) => {
  if (!ref) return null;
  return ref["@ref"]?.id || null;
};
