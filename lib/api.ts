import { myFetch } from "./hooks/request";
import { endpoints } from "./router";

export const copyList = async (fromListId, toListId) => {
  return await myFetch<any>(endpoints.listsItemsCopy(fromListId), {
    method: "POST",
    body: JSON.stringify({
      destinationListId: toListId,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  });
};
