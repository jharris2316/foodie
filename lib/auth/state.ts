import { IUser } from "lib/models/user.model";
import { atom, selector } from "recoil";

export interface ISession {
  user: IUser;
  resolved: boolean;
}

export const initialSessionState: ISession = {
  user: {
    id: null,
    name: "",
    email: "",
    group: "",
  },
  resolved: false,
};

export const sessionState = atom<ISession>({
  key: "session",
  default: { ...initialSessionState },
});

export const isLoggedInSelector = selector({
  key: "isLoggedIn",
  get: ({ get }) => {
    const session = get(sessionState);
    return Boolean(session?.user?.id);
  },
});
