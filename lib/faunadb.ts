import faunadb from "faunadb";

/** Alias to `faunadb.query` */
export const q = faunadb.query;

/**
 * Creates an authenticated FaunaDB client
 * configured with the given `secret`.
 */
export function getClient(secret): faunadb.Client {
  return new faunadb.Client({ secret });
}

/**
 * Server secret key
 */
export const serverClient = new faunadb.Client({ secret: process.env.FAUNADB_SECRET_KEY });

/**
 * helper to get data and id from fauna ref
 */
export const transform = (i) => ({
  ...i.data,
  id: i?.ref?.id || null,
  ref: i?.ref || null,
});

export const documentsTransform = (data) => {
  return data.filter((d) => d?.ref?.id).map(transform);
};

// user authenticated key
export const authClient = (secret) =>
  new faunadb.Client({
    secret,
  });
