export const arrayMutateUpdater = (updateId: string, updateData: any) => (items) => {
  if (!items?.length) return [updateData];

  const index = items.findIndex((item) => item.id === updateId);
  return items.map((item, i) => {
    if (i !== index) return item;

    return updateData;
  });
};

export const arrayMutateCreator = (newItem, index = null) => (items) => {
  if (index === null) return [...items, newItem];

  items.splice(index, 0, newItem);

  return [...items];
};
export const arrayMutateDeleter = (deleteId) => (items) => items.filter((item) => item.id !== deleteId);
