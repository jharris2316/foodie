import { getSession } from "./auth-cookies";
import * as Validator from "validatorjs";
import { UserModel } from "lib/models/user.model";

const setUserSession = async (req, res) => {
  try {
    const session = await getSession(req);

    if (!session?.token || !session?.user || !session?.user?.id) {
      const error = new Error("User authentication failed");
      res.statusCode = 401;
      return res.end(error.message);
    }

    const userModel = new UserModel();
    const userRef = await userModel.getUserRef(session?.user?.id);

    req.token = session.token;
    req.currentUser = session.user;
    req.faunaUserRef = userRef;
  } catch (e) {
    console.error(e);
    res.statusCode = e?.requestResult?.statusCode || 401;
    res.end(e.message);
  }
};

export const authMiddlewareServerSideProps = (handler) => {
  return async (context) => {
    await setUserSession(context.req, context.res);
    return handler(context);
  };
};

export const authMiddleware = (handler) => {
  return async (req, res) => {
    await setUserSession(req, res);
    return handler(req, res);
  };
};

const sanitizeData = (data, fields = []) => {
  if (!fields?.length) return data;

  return Object.keys(data || {})
    .filter((key) => fields.includes(key))
    .reduce((obj, key) => {
      obj[key] = data[key];
      return obj;
    }, {});
};

export const validationMiddleware = (rules, fieldsWhitelist = []) => (handler) => {
  return (req, res) => {
    const sanitzed = sanitizeData(req.body, fieldsWhitelist);
    const validation = new Validator(sanitzed, rules);

    if (validation.fails()) {
      const errors = validation.errors.all();
      const errorString = Object.values(errors).join(" ");
      const error = new Error(errorString);
      res.statusCode = 422;
      return res.end(error.message);
    }

    req.sanitizedData = sanitzed;
    return handler(req, res);
  };
};
