import { myFetch } from "lib/hooks/request";

export const jsonFetcher = async (url) => {
  const data = await myFetch(url);
  return data;
};
